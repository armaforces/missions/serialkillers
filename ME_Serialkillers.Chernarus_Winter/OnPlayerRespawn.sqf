waituntil {(alive player)};
//Add action to change view distance
player addAction ["<t color='#c8c8ff'>Ustaw odległość widzenia</t>", {call AF_GUI_setViewDistanceGUI},nil, -10, false, true, "", "", 50];
if (side player == WEST) exitwith {
	//Set unit loadout to saved loadout
	player setUnitLoadout (player getVariable ["Saved_Loadout",[]]);	
};
if (side player == EAST) exitwith {
	removeallweapons player;
	//Start spectator and turn off local cache	
	["Initialize", [player, [], false, true, true, true, true, true, true, true]] call BIS_fnc_EGSpectator;
	Madin_cache = false;
};