/* RESCUE PRISONER
Teleports all prisoners (side EAST units) out of the prison and terminates spectator. After 120 seconds adds them to SK_Terro giving them time to run away from prison without green civilian dot, making tracking harder.

Params:
None

Return value:
None

Example:
spawn MDL_SK_fnc_jail_free;

Locality:
Runs local. Effect global.
*/

if (side player == EAST) then {
	{
		if (side _x == EAST) then {
			_x setpos (getpos jail_flag);
			["Terminate"] remoteExec ["BIS_fnc_EGSpectator",_x];
			["client\cache_local.sqf"] remoteExec ["execVM",_x];
			sleep 0.1;
			[_x] spawn {
				sleep (random [60,140,180]);
				private _unit = _this select 0;
				if (!(alive _unit)) exitWith {};
				SK_Terro pushBack _unit;
				publicVariable "SK_Terro";
			}
		};
	} forEach list jail;
};