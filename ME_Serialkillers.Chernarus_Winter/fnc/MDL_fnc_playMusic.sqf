/* 
Plays music for all clients and prevents interruption.

Params:
0: <ARRAY> - [Music classname, duration]

Return value:
None

Example:
["COD_MW_Mile_High_Club",146] spawn MDL_fnc_playMusic;

Locality:
Runs on local. Effect global.
*/
params ["_music","_duration"];

waitUntil {!SK_music_playing};
_music remoteExec ["playMusic",0];
SK_music_playing = true;
sleep _duration;
SK_music_playing = false;

