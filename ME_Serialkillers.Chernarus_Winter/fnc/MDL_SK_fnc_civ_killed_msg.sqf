/* SHOW KILL MSG
Show civilian killed sideChat message to BLUFOR.

Params:
0: <STRING> - time (hh:mm:ss)
1: <?> - location/town name
2: <NUMBER> - distance from nearest town
3: <OBJECT> - unit that killed civilian

Return value: 
None

Example:
[_time, _nearest_town, _distance] remoteExec ["MDL_SK_fnc_civ_killed_msg",0];

Locality:
Runs on every client.
*/

params ["_time","_nearest_town","_distance","_killer"];
// Check if distance is greater than 250 m. If so then change output a bit to represent that.
if (side _killer == WEST) then {
	if (_distance <= 250) then {
	[WEST, "HQ"] sidechat format ["Cywil został zabity przez policjanta o godzinie %1 w %2!",_time, text _nearest_town];
	} else {
	[WEST, "HQ"] sidechat format ["Cywil został zabity przez policjanta o godzinie %1 w pobliżu %2!",_time,text _nearest_town];
	};
} else {
	if (_distance <= 250) then {
	[WEST, "HQ"] sidechat format ["Cywil został zabity o godzinie %1 w %2!",_time, text _nearest_town];
	} else {
	[WEST, "HQ"] sidechat format ["Cywil został zabity o godzinie %1 w pobliżu %2!",_time,text _nearest_town];
	};
};