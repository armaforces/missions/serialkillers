/* CIVILIAN VEHICLES ALARM MSG


Params:
None

Return value:
None

Example:


Locality:
Runs local. Effect local.
*/

params ["_nearest_town","_vehicle"];
//Play alarm sound that can be heard in 1000 m radius.
_vehicle say ["CarAlarm", 1000, 1.0];
// Show sideChat message.
[WEST, "HQ"] sidechat format ["Pojazd został ukradziony w pobliżu %1!",text _nearest_town];
[EAST, "HQ"] sidechat format ["W pojeździe włączył się alarm! JADO BAGIETY!",text _nearest_town];