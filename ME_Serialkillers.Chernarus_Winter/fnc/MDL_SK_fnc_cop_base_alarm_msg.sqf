/* SPAWNER ALARM MSG


Params:
0: <OBJECT> - object with unauthorized access detected

Return value:
None

Example:
[_object] spawn MDL_SK_fnc_cop_base_alarm_msg;

Locality:
Runs local. Effect global.
*/

params ["_object"];

titleText ["To był zły pomysł...", "PLAIN", 5];


private _nearest_town = [_object] call MDL_SK_fnc_nearest_town;
sleep (random 5);
// Show sideChat message to everyone.
private _msg = format ["Ktoś majstruje coś w bazie policji w %1!",text _nearest_town];
[[WEST, "HQ"], _msg] remoteExec ["sideChat",0];