/* 
Opens GUI for view distance change.

Params:
None

Return value:
None

Example:
call AF_GUI_setViewDistanceGUI;

Locality:
Local effect.
*/

disableSerialization;
createDialog "view_distance_settings";
waitUntil {!isNull (findDisplay 10);};
ctrlSetText [311, str viewdistance];