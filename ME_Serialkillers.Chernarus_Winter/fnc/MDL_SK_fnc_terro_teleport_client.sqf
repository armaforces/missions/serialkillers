/* TERRO TELEPORT INIT
Initializes teleportation actions on client for all available teleport locations.
Added action teleports player to selected location and removes all teleport location's markers.


Params:
None

Return value:
None

Example:
[_flag] remoteExec ["MDL_SK_fnc_terro_teleport_client",0];

Locality:
Runs local. Effect global.
*/

_flag = terroTeleportFlag;
{
_loc_pos = _x select 0;
_loc_name = _x select 1;
_flag addAction [_loc_name, {
	_loc_pos = _this select 3;
	player setpos _loc_pos;
	// Call function to remove teleport locations markers.
	call MDL_SK_fnc_terro_teleport_position_marker_remove;
}, _loc_pos, 6, true, true, "", "true", 5];
} forEach SK_Terro_spawns;