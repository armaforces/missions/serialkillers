/* COP KILLED
Handles events after terrorist death.

Params:
0: <OBJECT> - terrorist unit that was killed

Return value:
None

Example:
[player] remoteExec ["MDL_SK_fnc_terro_killed",2];

Locality:
Runs on server. Effect global.
*/

params ["_unit"];
//// Remove unit from SK_Terro array.
SK_Terro = SK_Terro - [_unit];
publicvariable "SK_Terro";