/* 
Checks if all OPFOR players are in jail

Params:
None

Return value:
0: <BOOLEAN>

Example:
_terro_status = call MDL_SK_fnc_terro_check

Locality:
Runs local. Effect local.
*/


_present_terros = EAST countSide AllUnits;
_killed_terros = EAST countSide list jail;


if (_present_terros <= _killed_terros) then {
true
} else {
false
};