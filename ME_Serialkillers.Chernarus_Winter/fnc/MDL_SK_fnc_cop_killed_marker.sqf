/* SHOW COP KILL MARKER
Create global marker at killed police unit's position.

Params:
0: <STRING> - marker text
1: <OBJECT> - killed unit

Return value:
None

Example:
[_time, _unit] call MDL_SK_fnc_cop_killed_marker;

Locality:
Runs on server only. Effect global.
*/

params ["_time","_unit"];
private _marker_name = format ["killed_cop_%1",random (999)];
private _marker_text = format ["%1", _time];
private _marker = createMarker [_marker_name, getpos _unit];
_marker setMarkerType "mil_objective";
_marker setMarkerColor "ColorWEST";
_marker setMarkerSize [0.4,0.4];
_marker setMarkerText _marker_text;

[_marker] spawn {
	private _marker = _this select 0;
	private _i = 1;
	while {_i > 0} do {
		sleep 15;
		_i = _i - 0.020;
		_marker setMarkerAlpha _i;
	};
	deleteMarker _marker;
};