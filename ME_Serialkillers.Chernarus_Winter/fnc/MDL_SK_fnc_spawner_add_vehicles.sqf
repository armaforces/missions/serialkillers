/* VEHICLE SPAWNER UPDATE
Adds vehicles that should be unlocked to vehicle spawn actions list.

Params:
0: <OBJECT> - object which has spawn action assigned

Return value:
None

Example:
[_spawner] spawn MDL_SK_fnc_spawner_add_vehicles;

Locality:
Runs local. Effect local.
*/

params ["_spawner"];
// Check if we are adding land vehicles or helicopters.
if (typeOf _spawner == vehicle_spawn_point) then {
	// Loop through SK_Vehicles array to find and add all vehicles that meet unlocking criteria (SK_BLUFOR_Score >= score_needed)
	{
		// Select _condition (_score_needed) from SK_Vehicles ([_score_needed, _vehicle_name, _vehicle_classname]) entry and compare it to SK_BLUFOR_Score)
		_condition = _x select 0;
		if (SK_BLUFOR_Score < _condition) exitwith {};
		_vehicle_name = _x select 1;
		_vehicle_classname = _x select 2;
		// Add action to _spawner that spawns _vehicle_classname.
		_spawner addAction [_vehicle_name, {
			_spawner = _this select 0;
			// Find nearby helipads and select nearest as vehicle spawn point.
			_helipad = ((nearestObjects [_spawner, [vehicle_spawn_location],50]) select {typeOf _x == vehicle_spawn_location}) select 0;
			// Clear spawn point area.
			_clear = nearestObjects [_helipad, ["AllVehicles"], 10];
			{[_x] remoteExec ["deleteVehicle",2];} forEach _clear;
			waitUntil {sleep 0.5; (count (nearestObjects [_helipad, ["Car","Tank","Air"], 10])) == 0};
			// Spawn vehicle and set direction
			_vehicle_classname = _this select 3;
			_veh = _vehicle_classname createVehicle (getpos _helipad);
			_veh setDir (getDir _helipad);
			sleep 0.5;
			// Clear vehicle cargo
			clearMagazineCargoGlobal _veh;
			clearWeaponCargoGlobal _veh;
			clearItemCargoGlobal _veh;
//			[_veh] remoteExec ["MDL_SK_fnc_police_car_action",0];
			// Add utilities to cargo
			if (_veh isKindOf "Tank") then {
				["ACE_Track", _veh] call ace_cargo_fnc_loadItem;
			} else {
				["ACE_Wheel", _veh] call ace_cargo_fnc_loadItem;
				["ACE_Wheel", _veh] call ace_cargo_fnc_loadItem;
				["ACE_Wheel", _veh] call ace_cargo_fnc_loadItem;
			};
		}, _vehicle_classname, 6, true, true, "", "true", 2];
	} forEach SK_Vehicles;
} else {
	{
		_condition = _x select 0;
		if (SK_BLUFOR_Score < _condition) exitwith {};
		_vehicle_name = _x select 1;
		_vehicle_classname = _x select 2;
		// Add action to _spawner that spawns _vehicle_classname.
		_spawner addAction [_vehicle_name, {
			_spawner = _this select 0;
			// Find nearby helipads and select nearest as vehicle spawn point.
			_helipad = ((nearestObjects [_spawner, [heli_spawn_location],50]) select {typeOf _x == heli_spawn_location}) select 0;
			// Clear spawn point area.
			_clear = nearestObjects [_helipad, ["AllVehicles"], 10];
			{[_x] remoteExec ["deleteVehicle",2];} forEach _clear;
			waitUntil {sleep 0.5; (count (nearestObjects [_helipad, ["Car","Tank","Air"], 10])) == 0};
			// Spawn vehicle and set direction
			_vehicle_classname = _this select 3;
			_veh = _vehicle_classname createVehicle (getpos _helipad);
			_veh setDir (getDir _helipad);
			sleep 0.5;
			// Clear vehicle cargo
			[_veh] remoteExec ["clearMagazineCargo",0];
			[_veh] remoteExec ["clearWeaponCargo",0];
			clearItemCargoGlobal _veh;
		}, _vehicle_classname, 6, true, true, "", "true", 2];
	} forEach SK_Helis;
};