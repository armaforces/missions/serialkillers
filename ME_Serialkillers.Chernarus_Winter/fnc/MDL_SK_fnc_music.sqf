/* 
Handles music during the game

Params:
None

Return value:
None

Example:

Locality:
Runs on server only. Effect global.
*/

waitUntil {40 <= SK_OPFOR_Score};
private _score = +SK_OPFOR_Score;
sleep 45;
if (SK_OPFOR_Score - _score >= 2 OR SK_OPFOR_Score > 45) then {
	private _music = selectRandom [
	["COD_BO2_Escape_From_Anthem",105],
	["COD_MW_Mile_High_Club",146],
	["COD_MW2_Cliffhanger_Snowmobile_Chase",282],
	["COD_MW2_Ending_Part_1",148],
	["COD_MW2_Makarovs_Safehouse_Escape",120],
	["COD_BO2_Escape_From_Anthem",105]
	];
	_music spawn MDL_fnc_playMusic;
} else {
	private _music = selectRandom [
	["COD_MW2_Boneyard_Fly_By",190],
	["COD_MW2_Contingency",300],
	["COD_MW2_Takedown_Part_3",343],
	["COD_MW3_Paris_Siege",311]
	];
	_music spawn MDL_fnc_playMusic;
};
[[WEST, "HQ"],selectRandom ["US_1mc_losing_fight_01","US_1mc_losing_fight_02"]] remoteExec ["sideRadio",0];
[[EAST, "HQ"],selectRandom ["RU_1mc_keepfighting_02","RU_1mc_winning_02"]] remoteExec ["sideRadio",0];