/* ARSENAL UPDATE
Adds equipment to arsenal.

Params: 
0: <OBJECT> - object with arsenal assigned.
1: <ARRAY> - [score_needed_to_unlock, weapon_classname, magazine_classname]

Return value:
None

Example:
[_arsenal,_Weapons] spawn MDL_SK_fnc_arsenal_add_items;

Locality:
Runs on every client independently. Local effect.
*/

params ["_arsenal","_Weapons"];
// Loop through _Weapons array to find all weapon_classname and magazine_classname.
{
	[_arsenal,_x select 1,true] call ace_arsenal_fnc_addVirtualItems;
} forEach _Weapons;
// Add weapons and magazines to arsenal.