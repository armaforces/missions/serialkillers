/* TERRO FLAG CREATION
Creates teleport flag at EAST starting location and remotely executes function to add teleportation actions on every client.

Params:
None

Return value:
None

Example:
call MDL_SK_fnc_terro_teleport_server;

Locality:
Runs on server only. Effect global.
*/

_flag = teleport_flag_terro createVehicle getpos terro_spawn;
terroTeleportFlag = _flag;
publicVariable "terroTeleportFlag";