/*Cop equipment presets
0: <NUMBER> - Score Needed
1: <STRING> - Preset name
2: <ARRAY> - clothes
20: <STRING> - uniform
21: <STRING> - vest
22: <STRING> - backpack
23: <STRING> - helmet
3: <ARRAY> - weapons
30: <ARRAY> - primary
300: <STRING> - primary weapon
301: <STRING> - primary weapon magazines
31: <ARRAY> - secondary
310: <STRING> - secondary weapon
311: <STRING> - secondary weapon magazines
32: <ARRAY> - launcher
320: <STRING> - launcher weapon
321: <STRING> - launcher weapon magazines
*/

/*
	Policjant
	["rds_uniform_Policeman","rds_pistol_holster","","rds_police_cap"],
	Lepszy policjant
	["LOP_U_UVF_Fatigue_GREY_GSW","V_TacVest_blk","","LOP_H_6B27M_ANA"],
	SWAT
	["RM_SWAT_Uniform_02","RM_SWAT_Vest_01","","RM_SWAT_Helmet_01"],
*/
_SK_Cop_Presets = [
//	[0,"presetname",
//	["uniform","vest","backpack","helmet"],
//	[["primaryweapon",
//	["primaryweaponmags"]],
//	["secondaryweapon",
//	["secondaryweaponmag"]],
//	["launcher",
//	["launchermag"]]]
//	],

	//M9 15rnd x5
	[0,"Bieda policjant M9",
	["rds_uniform_Policeman","rds_pistol_holster","","rds_police_cap"],
	[["",
	[""]],
	["rhsusf_weap_m9",
	["rhsusf_mag_15Rnd_9x19_JHP","rhsusf_mag_15Rnd_9x19_JHP","rhsusf_mag_15Rnd_9x19_JHP","rhsusf_mag_15Rnd_9x19_JHP","rhsusf_mag_15Rnd_9x19_JHP"]],
	["",
	[""]]]],
	
	//Glock 17rnd x5
	[0,"Bieda policjant Glock",
	["rds_uniform_Policeman","rds_pistol_holster","","rds_police_cap"],
	[["",
	[""]],
	["rhsusf_weap_glock17g4",
	["rhsusf_mag_17Rnd_9x19_JHP","rhsusf_mag_17Rnd_9x19_JHP","rhsusf_mag_17Rnd_9x19_JHP","rhsusf_mag_17Rnd_9x19_JHP","rhsusf_mag_17Rnd_9x19_JHP"]],
	["",
	[""]]]],

	// Strzelba 8rnd x5 + Glock 17rnd x3 + 1 smoke
	[10,"Policjant Shotgun + Glock",
	["LOP_U_UVF_Fatigue_GREY_GSW","V_TacVest_blk","","LOP_H_6B27M_ANA"],
	[["rhs_weap_M590_8RD",
	["rhsusf_8Rnd_00Buck","rhsusf_8Rnd_00Buck","rhsusf_8Rnd_00Buck","rhsusf_8Rnd_00Buck","rhsusf_8Rnd_00Buck"]],
	["rhsusf_weap_glock17g4",
	["rhsusf_mag_17Rnd_9x19_JHP","rhsusf_mag_17Rnd_9x19_JHP","rhsusf_mag_17Rnd_9x19_JHP"]],
	["",
	["rhs_mag_an_m8hc"]]]],

	// MP5A4 30rnd x5 + Glock 17rnd x3 + 1 smoke
	[15,"Policjant MP5A4 + Glock",
	["LOP_U_UVF_Fatigue_GREY_GSW","V_TacVest_blk","","LOP_H_6B27M_ANA"],
	[["hlc_smg_mp5a4",
	["hlc_30Rnd_9x19_B_MP5","hlc_30Rnd_9x19_B_MP5","hlc_30Rnd_9x19_B_MP5","hlc_30Rnd_9x19_B_MP5","hlc_30Rnd_9x19_B_MP5"]],
	["rhsusf_weap_glock17g4",
	["rhsusf_mag_17Rnd_9x19_JHP","rhsusf_mag_17Rnd_9x19_JHP","rhsusf_mag_17Rnd_9x19_JHP"]],
	["",
	["rhs_mag_an_m8hc"]]]],
	
	// MP5A4 30rnd x5 + Glock 17rnd x3 + 1 smoke + 3 flashbang
	[20,"SWAT MP5SD6 + Glock",
	["RM_SWAT_Uniform_02","RM_SWAT_Vest_01","","RM_SWAT_Helmet_01"],
	[["hlc_smg_mp5a4",
	["hlc_30Rnd_9x19_B_MP5","hlc_30Rnd_9x19_B_MP5","hlc_30Rnd_9x19_B_MP5","hlc_30Rnd_9x19_B_MP5","hlc_30Rnd_9x19_B_MP5"]],
	["rhsusf_weap_glock17g4",
	["rhsusf_mag_17Rnd_9x19_JHP","rhsusf_mag_17Rnd_9x19_JHP","rhsusf_mag_17Rnd_9x19_JHP"]],
	["",
	["rhs_mag_an_m8hc","rhs_mag_mk84","rhs_mag_mk84","rhs_mag_mk84"]]]],
	
	// M16A4 30rnd x5 + Glock 17rnd x3 + 1 smoke + 3 flashbang
	[20,"SWAT M16A4 + Glock",
	["RM_SWAT_Uniform_02","RM_SWAT_Vest_01","","RM_SWAT_Helmet_01"],
	[["rhs_weap_m16a4_carryhandle",
	["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red","rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red","rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red","rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red","rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
	["rhsusf_weap_glock17g4",
	["rhsusf_mag_17Rnd_9x19_JHP","rhsusf_mag_17Rnd_9x19_JHP","rhsusf_mag_17Rnd_9x19_JHP"]],
	["",
	["rhs_mag_an_m8hc","rhs_mag_mk84","rhs_mag_mk84","rhs_mag_mk84"]]]],
	
	[100,"presetname",["uniform","vest","backpack","helmet"],[["primaryweapon",["primaryweaponmags"]],["secondaryweapon",["secondaryweaponmag"]],["launcher",["launchermag"]]]]
];