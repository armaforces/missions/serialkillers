/* TERRO SPECIAL AMMO CRATE PLACEMENT
Randomly select location from SK_Terro_spawns to teleport ammo crate to and remotely create local markers on client on crate's position.

Params:
None

Return value:
None

Example:
spawn MDL_SK_fnc_terro_special_ammo;

Locality:
Runs on server only. Effect global.
*/

_crate = special_ammo;
// Select random location to teleport crate to.
_location = SK_Terro_spawns call BIS_fnc_selectRandom;
_position = _location select 0;
_crate setPos _position;
// RemoteExec function on every client to create marker on ammo crate's position.
[_crate] remoteExec ["MDL_SK_fnc_terro_special_ammo_marker",0];

{
	_wpnCount = _x select 0;
	_ammoCount = _x select 1;
	_weapon = _x select 2;
	_ammo = _x select 3;
	_crate addItemCargoGlobal [_weapon, _wpnCount];
	_crate addItemCargoGlobal [_ammo, _ammoCount];
} forEach SK_SpecialWeapons;