/* TERRO TELEPORT POSITION MARKER REMOVAL TOOL
Removes teleport positions markers after player had teleported to one of them.

Params:
None

Return value:
None

Example:
call MDL_SK_fnc_terro_teleport_position_marker_remove;

Locality:
Runs local. Effect local.
*/

{
	deleteMarkerlocal _x;
} forEach SK_Terro_spawns_markers;