/* VEHICLE SPAWNER RELOAD
Action to reload vehicle spawn actions list.

Params:
0: <OBJECT> - object which has spawn action assigned

Return value:
None

Example:
[_spawner] spawn MDL_SK_fnc_spawner_reload;

Locality:
Runs local. Effect local.
*/
params ["_spawner"];
// Remove all actions so new one can be added.
_side = player getVariable "Player_Side";
if (_side != WEST) exitwith {
	[_spawner] spawn MDL_SK_fnc_cop_base_alarm_msg;
};
removeAllActions _spawner;
// Check if _spawner is a vehicle_spawn_point or _heli_spawn_point
// and add action to refresh actions list.
if (typeOf _spawner == vehicle_spawn_point) then {
	_spawner addAction ["Odśwież menu pojazdów", {
	[_this select 0,false] spawn MDL_SK_fnc_spawner_reload;
	}, {}, 6, true, true, "", "true", 2];
} else {
	_spawner addAction ["Odśwież menu helikopterów", {
	[_this select 0,true] spawn MDL_SK_fnc_spawner_reload;
	}, {}, 6, true, true, "", "true", 2];
};
// Spawn function to add vehicles spawn action to actions list.
[_spawner] spawn MDL_SK_fnc_spawner_add_vehicles;