/* COP PRESET SELECT
Checks which predefined presets are unlocked.

Params:
0: <OBJECT> - object to assign actions to

Return value:
None

Example:
[player] call MDL_SK_fnc_cop_preset_select_check;

Locality:
Runs local. Effect local.
*/

params ["_unit"];

#include "config\cop_presets.sqf";
// Loop through cop bases to add teleport actions for each cop base location.
private _i = -1;
{
	if (_x select 0 > SK_BLUFOR_Score) exitwith {};
	_i = _i + 1;
} forEach _SK_Cop_Presets;

if (_i == -1) exitwith {};
[_unit,_i] spawn MDL_SK_fnc_cop_preset_select_add;