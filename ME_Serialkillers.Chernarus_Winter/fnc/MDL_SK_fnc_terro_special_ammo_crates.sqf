/* TERRO SPECIAL AMMO CRATE PLACEMENT
Fills terro ammo crates with random gear and remotely create local markers on client on crate's position.

Params:
None

Return value:
None

Example:
spawn MDL_SK_fnc_terro_special_ammo_crates;

Locality:
Runs on server only. Effect global.
*/

private _Packs = [
[[1,2],SK_SpecialWeapons],
[[2,4],SK_SpecialPistols],
[[1,4],SK_SpecialLaunchers],
[[2,5],SK_SpecialRifles],
[[4,10],SK_SpecialItems],
[[1,8],SK_SpecialClothesCivilian],
[[0,2],SK_SpecialClothesMilitary]
];

{
	private _crate = _x;
	// RemoteExec function on every client to create marker on ammo crate's position.
	[_crate] remoteExec ["MDL_SK_fnc_terro_special_ammo_marker",0,true];
	{
		private _i = 0;
		private _pack = _x select 1;
		private _packMax = _x select 0 select 1;
		private _packMin = _x select 0 select 0;
		private _max = floor(random [_packMin,(_packMax+_packMin+1)/2,_packMax+1]);
		while {_i < _max} do {
			private _Gear = selectRandom _pack;
			_crate addItemCargoGlobal [_Gear select 0 select 1, _Gear select 0 select 0];
			private _Mags = _Gear select 1;
			{
				_crate addItemCargoGlobal [_x select 1, _x select 0];
			} forEach _Mags;
			_i = _i + 1;
		};
	} forEach _Packs;
	//Disable box damage and add EH to punish cops if they open the box
	_x allowDamage false;
	_x addEventHandler ["ContainerOpened", {
		params ["_container","_unit"];
		if (side _unit == WEST) then {
			[_unit, nil, true] spawn BIS_fnc_moduleLightning;
			[-1,0] spawn MDL_SK_fnc_score_change;
		};
	}];
} forEach SK_Terro_ammoCrates;