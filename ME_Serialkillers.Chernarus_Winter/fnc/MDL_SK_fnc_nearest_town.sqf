/* FIND NEAREST TOWN
Find nearest town.

Params:
0: <OBJECT> - unit

Return value: 
<STRING> - nearest town/village name 

Example:
_nearest_town = [_unit] call MDL_SK_fnc_nearest_town;

Locality:
Runs local.
*/

params ["_unit"];
// Get nearest locations to object.
private _towns = nearestLocations [getPosATL _unit, ["NameVillage","NameCity","NameCityCapital"], 2000];
// Select nearest location from array.
private _nearest_town = _towns select 0;
_nearest_town;