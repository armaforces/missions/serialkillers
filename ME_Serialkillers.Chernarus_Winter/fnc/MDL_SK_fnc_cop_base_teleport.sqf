/* COP TELEPORTS
Creates actions on object to teleport to all cop bases.

Params:
0: <OBJECT> - teleporter (flag)

Return value:
None

Example:
[_flag] call MDL_SK_fnc_cop_base_teleport;

Locality:
Runs local. Effect local.
*/

params ["_flag"];
// Loop through cop bases to add teleport actions for each cop base location.
{
	private _loc_pos = _x select 0;
	private _loc_name = _x select 1;
	_flag addAction [_loc_name, {
		private _side = player getVariable "Player_Side";
		if (_side != WEST) exitwith {[_this select 1] spawn MDL_SK_fnc_cop_base_alarm_msg};
		_loc_pos = _this select 3;
		player setpos _loc_pos;
	}, _loc_pos, 6, true, true, "", "true", 5];
} forEach SK_Cop_bases;