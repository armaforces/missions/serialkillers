/* RETURN IN-GAME TIME
Return in-game time in format hh:mm:ss.

Params:
None

Return value:
<STRING> - Time in format hh:mm:ss.

Example:
call MDL_SK_fnc_time_clock;

Locality:
Runs local.
*/

// Get current time. Eg. 20.5 for 20:30
_daytime = daytime;
// Get _hour from _daytime by removing decimal part. Eg. 20 from 20.5.
_hour = floor _daytime;
// Substract full hours from _daytime to leave only minutes and seconds.
_daytime = _daytime - _hour;
// Get _minute from _daytime by removing decimal part after multiplying by 60 (number of minutes in hour).
_minute = floor(_daytime * 60);
// Substract full minutes from _daytime to leave only seconds.
_daytime = (_daytime * 60) - _minute;
// Add 0 in front of _minute if it's less than 10.
if (_minute <= 9) then {
	_minute = format ["0%1",_minute];
};
// Get _second from _daytime by removing decimal part after multiplying by 60 (number of seconds in minute).
_second = floor (_daytime * 60);
// Add 0 in front of _second if it's less than 10.
if (_second <= 9) then {
	_second = format ["0%1",_second];
};
// Create output.
_time24 = format ["%1:%2:%3", _hour, _minute, _second];
_time24