/* SCORE CHANGING
Changes score and broadcasts that change and corresponding message.

Params:
0: <NUMBER> - value added to BLUFOR score
1: <NUMBER> - value added to OPFOR score

Return value:
None

Example:
[1,1] spawn MDL_SK_fnc_score_change;

Locality:
Runs local. Effect global.
*/

params ["_change_blufor","_change_opfor"];
// Increase score.
private _time = call MDL_SK_fnc_time_clock;
SK_BLUFOR_Score = SK_BLUFOR_Score + _change_blufor;
SK_OPFOR_Score = SK_OPFOR_Score + _change_opfor;
publicvariable "SK_BLUFOR_Score";
publicvariable "SK_OPFOR_Score";
[WEST, _change_blufor] call BIS_fnc_respawnTickets;
[EAST, _change_opfor] call BIS_fnc_respawnTickets;
// Create message.
private _msg = format ["Godzina:%1\n\nWynik terrorystów: %2/%3\nZmiana: %4\n\nWynik policjantów: %5/%6\nZmiana: %7", _time, SK_OPFOR_Score, SK_OPFOR_Score_Max, _change_opfor, SK_BLUFOR_Score, SK_BLUFOR_Score_Max, _change_blufor];
// Broadcast message to all clients.
[_msg] remoteExec ["hintSilent",0];