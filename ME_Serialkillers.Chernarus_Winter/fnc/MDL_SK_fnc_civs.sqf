/* CIVILIAN INIT
Initialize civilian EH, markers.
Runs on server and HCs only.

spawn MDL_SK_fnc_civ_killed;

Params: none.

Returns: none.

*/

if (isServer OR !hasInterface) then {
	// Loop through allUnits to find all civilian units.
	{
		if (side _x == INDEPENDENT AND alive _x OR side _x == CIVILIAN AND alive _x) then {
			// Add unit to SK_Civis array.
			SK_Civis pushbackunique _x;
			private _EH_added = _x getVariable "SK_EH_added";
			if (isNil "_EH_added") then {
				// Set variable "EH_added" to true to prevent adding EH multiple times.
				_x setVariable ["SK_EH_added",true];
				// Add EH killed which spawns MDL_SK_fnc_civ_killed function on unit death.
				_x addEventHandler ["killed", {[_this select 0,_this select 1] spawn MDL_SK_fnc_civ_killed}];
				// Initialize variable SK_Killed.
				_x setVariable ["SK_Killed",false];
			};
		};
	} forEach AllUnits;
	publicVariable "SK_Civis";
};