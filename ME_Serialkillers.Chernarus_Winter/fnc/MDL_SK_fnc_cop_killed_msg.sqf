/* SHOW COP KILL MSG


Params:
0: <STRING> - time of event in format HH:MM:SS
1: <LOCATION> - nearest town location
2: <NUMBER> - distance from nearest town

Return value:
None

Example:
[_time, _nearest_town, _distance] remoteExec ["MDL_SK_fnc_cop_killed_msg",0];

Locality:
Runs on every client.
*/

params ["_time","_nearest_town","_distance"];
// Check if distance is greater than 250 m. If so then change output a bit to represent that.
if (_distance <= 250) then {
	[WEST, "HQ"] sidechat format ["Policjant został zabity o godzinie %1 w %2!", _time, text _nearest_town];
} else {
	[WEST, "HQ"] sidechat format ["Policjant został zabity o godzinie %1 w pobliżu %2!", _time, text _nearest_town];
};