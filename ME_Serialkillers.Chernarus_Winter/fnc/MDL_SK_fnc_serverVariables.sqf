/* 
Broadcasts all crucial variables to client

Params:
0: <NUMBER> - clientID

Return value:
None

Example:
_id = clientOwner;
[_id] remoteExec ["MDL_SK_fnc_serverVariables",2];

Locality:
Runs on server only. Effect global.
*/

params ["_id"];

_id publicVariableClient "SK_BLUFOR_Score";
_id publicVariableClient "SK_OPFOR_Score";
_id publicVariableClient "terroTeleportFlag";
_id publicVariableClient "CopTeleportFlags";
_id publicVariableClient "CopSpawners";
_id publicVariableClient "SK_Arsenals";
_id publicVariableClient "SK_Terro_spawns";
_id publicVariableClient "SK_Civis";
_id publicVariableClient "SK_snow";

SK_serverVariables = true;
_id publicVariableClient "SK_serverVariables";