/* CIVILIAN VEHICLE SPAWN
Spawn random civilian vehicle with persistent texture on all clients.

Params:
0: <OBJECT> - object like helipad to spawn vehicle on

Return value:
None

Example:
[_location] spawn MDL_SK_fnc_vehicle_spawn;

Locality:
Runs on server only. Effect global.
*/

params ["_location"];
private _location_pos = getpos _location;
deleteVehicle _location;
if (random 1 >= 0.99) exitwith {};
private _random_veh = SK_Civilian_Vehicles call BIS_fnc_selectRandom;
private _veh = _random_veh createVehicle _location_pos;
clearItemCargoGlobal _veh;
_veh setVariable ["BIS_enableRandomization", false];
private _i = 0;
private _wheels = floor(random 4);
while {_i <= _wheels} do {
	["ACE_Wheel", _veh] call ace_cargo_fnc_loadItem;
	_i = _i + 1;
};
if (typeOf _veh == "C_Offroad_01_F") then {
	_offroadTextures = [
	"po_vehicles\data\camo\offroad\offroad_black_co.paa",
	"po_vehicles\data\camo\offroad\offroad_blue_co.paa",
	"po_vehicles\data\camo\offroad\offroad_green_co.paa",
	"po_vehicles\data\camo\offroad\offroad_khk_co.paa",
	"po_vehicles\data\camo\offroad\offroad_orange_co.paa",
	"po_vehicles\data\camo\offroad\offroad_red_co.paa",
	"po_vehicles\data\camo\offroad\offroad_white_co.paa",
	"po_vehicles\data\camo\offroad\offroad_yellow_co.paa"
	];
	_texture = _offroadTextures call BIS_fnc_selectRandom;
	_veh setObjectTextureGlobal [0,_texture]
};
if (typeOf _veh == "C_Van_01_transport_F" OR typeOf _veh == "C_Van_01_fuel_F" OR typeOf _veh == "C_Van_01_box_F") then {
	_vanTextures = [
	"a3\soft_f_gamma\van_01\data\van_01_ext_red_co.paa",
	"a3\soft_f_gamma\van_01\data\Van_01_ext_CO.paa",
	"a3\soft_f_gamma\van_01\data\Van_01_ext_IG_CO.paa",
	"a3\soft_f_gamma\van_01\data\Van_01_ext_black_CO.paa",
	"a3\soft_f_gamma\van_01\data\Van_01_ext_ti_ca.paa"
	];
	_texture = _vanTextures call BIS_fnc_selectRandom;
	_veh setObjectTextureGlobal [0,_texture]
};
if (typeOf _veh == "C_Hatchback_01_F" OR typeOf _veh == "C_Hatchback_01_sport_F") then {
	_hatchbackTextures = [
	"a3\soft_f_gamma\hatchback_01\data\hatchback_01_ext_base01_co.paa",
	"a3\soft_f_gamma\hatchback_01\data\hatchback_01_ext_base02_co.paa",
	"a3\soft_f_gamma\hatchback_01\data\hatchback_01_ext_base03_co.paa",
	"a3\soft_f_gamma\hatchback_01\data\hatchback_01_ext_base04_co.paa",
	"a3\soft_f_gamma\hatchback_01\data\hatchback_01_ext_base05_co.paa",
	"a3\soft_f_gamma\hatchback_01\data\hatchback_01_ext_base06_co.paa",
	"a3\soft_f_gamma\hatchback_01\data\hatchback_01_ext_base07_co.paa",
	"a3\soft_f_gamma\hatchback_01\data\hatchback_01_ext_base08_co.paa"
	];
	_texture = _hatchbackTextures call BIS_fnc_selectRandom;
	_veh setObjectTextureGlobal [0,_texture]
};
if (typeOf _veh == "C_Quadbike_01_F") then {
	_quadTextures = [
	"a3\soft_f_beta\quadbike_01\data\quadbike_01_civ_red_co.paa",
	"a3\soft_f_beta\quadbike_01\data\Quadbike_01_CIV_BLACK_CO.paa",
	"a3\soft_f_beta\quadbike_01\data\Quadbike_01_CIV_BLUE_CO.paa",
	"a3\soft_f_beta\quadbike_01\data\Quadbike_01_CIV_WHITE_CO.paa"
	];
	_texture = _quadTextures call BIS_fnc_selectRandom;
	_veh setObjectTextureGlobal [0,_texture]
};
if (typeOf _veh == "C_SUV_01_F") then {
	_suvTextures = [
	"a3\soft_f_gamma\suv_01\data\suv_01_ext_04_co.paa",
	"a3\soft_f_gamma\suv_01\data\SUV_01_ext_02_CO.paa",
	"a3\soft_f_gamma\suv_01\data\SUV_01_ext_03_CO.paa",
	"a3\soft_f_gamma\suv_01\data\SUV_01_ext_04_CO.paa",
	"a3\soft_f_gamma\suv_01\data\SUV_01_ext_CO.paa"
	];
	_texture = _suvTextures call BIS_fnc_selectRandom;
	_veh setObjectTextureGlobal [0,_texture]
};
if (typeOf _veh == "RDS_Golf4_Civ_01") then {
	_golfTextures = [
	"rds_a2port_civ\vwgolf\data\vwgolf_body_black_co.paa",
	"rds_a2port_civ\vwgolf\data\vwgolf_body_co.paa",
	"rds_a2port_civ\vwgolf\data\vwgolf_body_blue_co.paa",
	"rds_a2port_civ\vwgolf\data\vwgolf_body_green_co.paa",
	"rds_a2port_civ\vwgolf\data\vwgolf_body_white_co.paa",
	"rds_a2port_civ\vwgolf\data\vwgolf_body_yellow_co.paa"
	];
	_texture = _golfTextures call BIS_fnc_selectRandom;
	_veh setObjectTextureGlobal [0,_texture]
};
if (typeOf _veh == "RDS_S1203_Civ_01") then {
	_skodaTextures = [
	"rds_a2port_civ\s1203\data\s1203_silver_co.paa",
	"rds_a2port_civ\s1203\data\s1203_orange_co.paa",
	"rds_a2port_civ\s1203\data\s1203_red_co.paa"
	];
	_texture = _skodaTextures call BIS_fnc_selectRandom;
	_veh setObjectTextureGlobal [0,_texture]
};
if (typeOf _veh == "RDS_Octavia_Civ_01") then {
	_octaviaTextures = [
	"rds_a2port_civ\octavia\data\car_body_blue_co.paa",
	"rds_a2port_civ\octavia\data\car_body_black_co.paa",
	"rds_a2port_civ\octavia\data\car_body_co.paa",
	"rds_a2port_civ\octavia\data\car_body_yellow_co.paa"
	];
	_texture = _octaviaTextures call BIS_fnc_selectRandom;
	_veh setObjectTextureGlobal [0,_texture]
};
_veh setDir (getDir _location);
_veh addEventHandler ["GetIn", {[_this select 0] call MDL_SK_fnc_vehicle_alarm}];
//	[_veh,["GetIn", {[_this select 0] call MDL_SK_fnc_vehicle_alarm}]] remoteExec ["addEventHandler",0];// ["GetIn", {[_this select 0] call MDL_SK_fnc_vehicle_alarm}];