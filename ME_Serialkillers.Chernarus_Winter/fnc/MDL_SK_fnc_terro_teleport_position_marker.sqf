/* TERRO TELEPORT POSITION MARKER
Create local marker (side EAST only) showing where to EAST can teleport from starting location.

Params:
None

Return value:
None

Example:


Locality:
Runs local. Effect local.
*/

if (side player == EAST) exitwith {
	_spawn_location = _this select 0 select 0;
	_spawn_name = _this select 0 select 1;
	_marker = createMarkerlocal [_spawn_name, _spawn_location];
	_marker setMarkerColorlocal "ColorEAST";
	_marker setMarkerSizelocal [0.5,0.5];
	_marker setMarkerTypelocal "mil_end";
	_marker setMarkerTextLocal _spawn_name;
	SK_Terro_spawns_markers pushback _marker;
};