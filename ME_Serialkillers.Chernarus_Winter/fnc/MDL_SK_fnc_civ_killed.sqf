/* CIVILIAN KILLED
Handles events after killing civilian.

Params:
0: <OBJECT> - civilian unit that was killed
1: <OBJECT> - killer

Return value:
None

Example:
[_unit, _killer] call MDL_SK_fnc_civ_killed;

Locality:
Runs on server only. Remotely executes message function on all clients.
*/

params ["_unit","_killer"];

// Check if unit was killed before to prevent triggering multiple times.
private _killed = _unit getVariable "SK_Killed";

if (!_killed) then {
	// Set variable to prevent triggering second time.
	_unit setVariable ["SK_Killed",true];
	// Remove unit from array of Civilian units.
	SK_Civis = SK_Civis - [_unit];
	publicvariable "SK_Civis";
	// Get current time.
	private _time = [] call MDL_SK_fnc_time_clock;
	// Call function to create marker at killed unit's position.
	[_time, _unit, _killer] call MDL_SK_fnc_civ_killed_marker;
	// Get nearest town for message purposes.
	private _nearest_town = [_unit] call MDL_SK_fnc_nearest_town;
	// Estimate how far unit was from nearest town for message purposes.
	private _distance = (getpos _unit) distance (getpos _nearest_town);
	// Remotely execute function on clients to show message with collected data.
	[_time, _nearest_town, _distance, _killer] remoteExec ["MDL_SK_fnc_civ_killed_msg",0];
	// Spawn function to change score.
	if (side _killer == WEST) then {
		[-1,1] spawn MDL_SK_fnc_score_change;
	} else {
		[1,1] spawn MDL_SK_fnc_score_change;
	};
	SK_Civ_Killed = true;
};