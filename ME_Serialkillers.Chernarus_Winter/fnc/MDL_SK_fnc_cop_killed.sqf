/* COP KILLED
Handles events after killing policeman (or his suicide).

Params:
0: <OBJECT> - police unit that was killed
1: <OBJECT> - unit that killed

Return value:
None

Example:
[player, otherPlayer] remoteExec ["MDL_SK_fnc_cop_killed",2];

Locality:
Runs on server. Remotely executes message function on all clients.
*/

params ["_unit","_killer"];
//// Remove unit from SK_Cops array.
//SK_Cops = SK_Cops - [_unit];
//publicvariable "SK_Cops";

// Get current time.
private _time = [] call MDL_SK_fnc_time_clock;
// Call function to create marker at killed unit's position.
[_time, _unit] call MDL_SK_fnc_cop_killed_marker;
// Get nearest town for message purposes.
private _nearest_town = [_unit] call MDL_SK_fnc_nearest_town;
// Estimate how far unit was from nearest town for message purposes.
private _distance = (getpos _unit) distance (getpos _nearest_town);
// Remotely execute function on clients to show message with collected data.
[_time, _nearest_town, _distance] remoteExec ["MDL_SK_fnc_cop_killed_msg",0];
// Check why unit died and spawn funcion to change score.
if (side _killer == EAST OR _unit == _killer) then {
	[2,2] spawn MDL_SK_fnc_score_change;
} else {
	[0,2] spawn MDL_SK_fnc_score_change;
};