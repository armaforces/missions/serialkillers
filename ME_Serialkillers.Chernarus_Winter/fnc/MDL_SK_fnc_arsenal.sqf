/* ARSENALS HANDLER
Checks if required score is achieved to add items to arsenal.

Params:
None

Return value:
None

Example:
spawn MDL_SK_fnc_arsenal;

Locality:
Runs on every client independently. Local effect.
*/

private _i = 0;
while {_i <= 40} do {
	// Wait until achieved score exceeds current loop index.
	waitUntil {SK_BLUFOR_Score >= _i};
	// Select all entries from SK_PoliceWeapons which 1st element (score_needed) equals current loop index.
	_Weapons = SK_PoliceWeapons select {_x select 0 == _i};
	// Loop through all arsenals and spawn function to add selected items to them.
	{
	_arsenal = _x;
	[_arsenal, _Weapons] spawn MDL_SK_fnc_arsenal_add_items;
	} forEach SK_Arsenals;
	_i = _i + 1;
};