/* CIVILIAN VEHICLES INIT
Finds all civilian vehicle spawn locations.

Params:
None

Return value:
None

Example:
call MDL_SK_fnc_vehicle_spawn_init;

Locality:
Runs on server only. Effect local.
*/

_spawn_locations = allMissionObjects "VR_Area_01_circle_4_grey_F";
{
	_location = _x;
	[_location] spawn MDL_SK_fnc_vehicle_spawn;
} forEach _spawn_locations;