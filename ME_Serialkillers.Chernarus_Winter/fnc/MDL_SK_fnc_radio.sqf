/* 
Handles radio messages during the game

Params:
None

Return value:
None

Example:

Locality:
Runs on server only. Effect global.
*/

[] spawn {
	private _msg_played = false;
	while {!_msg_played} do {
		private _in_jail = EAST countSide list jail;
		private _present = EAST countSide AllUnits;
		if (_in_jail/_present >= 0.5) then {
			_msg_played = true;
			[[WEST, "HQ"],"US_1mc_winning_fight_01"] remoteExec ["sideRadio",0];
			private _radio = selectRandom["RU_1mc_losing_fight_01","RU_1mc_losing_fight_02"];
			[[EAST, "HQ"],_radio] remoteExec ["sideRadio",0];
			private _last_one = false;
			SK_radio_terro_losing = true;
			sleep 2;
			while {!_last_one} do {
				_in_jail = EAST countSide list jail;
				_present = EAST countSide AllUnits;
				if (_present - _in_jail == 1) then {
					_last_one = true;
					[[WEST, "HQ"],"US_1mc_one_enemy_01"] remoteExec ["sideRadio",0];
					_radio = selectRandom ["RU_1mc_lastalive_01","RU_1mc_lastalive_02","RU_1mc_lastalive_03","RU_1mc_lastalive_04","RU_1mc_lastalive_05"];
					[[EAST, "HQ"],_radio] remoteExec ["sideRadio",0];
				};
				sleep 1;
			};
		};
		sleep 1;
	};
};

/*
[] spawn {
	waitUntil {SK_BLUFOR_SCORE >= 40 OR SK_OPFOR_SCORE >= 40};
	_winning_side = WEST;
	if (SK_OPFOR_SCORE > SK_BLUFOR_SCORE AND !SK_radio_terro_losing) then {
		_winning_side = EAST;
	} else {
		[[WEST, "HQ"],""] remoteExec ["sideRadio",0];
		[[EAST, "HQ"],""] remoteExec ["sideRadio",0];
	};
};
*/