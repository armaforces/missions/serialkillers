/* TERRO SPECIAL AMMO CRATE MARKER
Create marker at special ammo crate's position (side EAST only).

Params:
0: <OBJECT> - special ammo crate

Return value:
None

Example:
[_crate] remoteExec ["MDL_SK_fnc_terro_special_ammo_marker",0];

Locality:
Runs local. Effect local.
*/

params ["_crate"];
// Check if player is on EAST side and create marker for him.
if (side player == EAST) exitwith {
	private _marker_text = format ["special_ammo_crate_%1", random 999];
	private _marker = createMarkerlocal [_marker_text, getpos _crate];
	_marker setMarkerColorlocal "ColorEAST";
	_marker setMarkerTypelocal "mil_unknown";
	_marker setMarkerSizeLocal [0.5,0.5];
};