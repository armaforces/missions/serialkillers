/* TERRO AMMO CRATE FILL
Fills terro ammo crate with weapons.

Params:
None

Return value:
None

Example:
spawn MDL_SK_fnc_terro_ammo;

Locality:
Runs on server only. Effect global.
*/

_crate = terro_ammo;

{
	_Gear = _x;
	_crate addItemCargoGlobal [_Gear select 0 select 1, _Gear select 0 select 0];
	private _Mags = _Gear select 1;
	{
		_crate addItemCargoGlobal [_x select 1, _x select 0];
	} forEach _Mags;
} forEach SK_TerroWeapons;