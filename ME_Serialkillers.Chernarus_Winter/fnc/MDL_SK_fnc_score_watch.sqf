/* 
Checks if opfor score has not changed for desired time and increases BLUFOR score.

Params:
None

Return value:
None

Example:
spawn MDL_SK_fnc_score_watch;

Locality:
Runs on server only.
*/

["Minęło 10 minut od startu. Wynik policji będzie zwiększany o 5 co 5 minut podczas których nie zwiększył się wynik terrorystów."] remoteExec ["hint",0];

private _i = 60;
private _penalty = 0;
private _penaltyLimit = 3;
while {_i >= 0} do {
	private _time = 300; //Time without OPFOR score changing to increase BLUFOR score by 5
	private _score = SK_OPFOR_Score;
	while {_time != 0 AND _score == SK_OPFOR_Score} do {
		_time = _time - 1;
		uisleep 1;
	};
	if (_score == SK_OPFOR_Score) then {
		[5,0] spawn MDL_SK_fnc_score_change;
		_penalty = _penalty + 1;
		if (_penalty >= _penaltyLimit) then {
			[WEST] call MDL_SK_fnc_end;
		};
	};
	_i = _i - 1;
};