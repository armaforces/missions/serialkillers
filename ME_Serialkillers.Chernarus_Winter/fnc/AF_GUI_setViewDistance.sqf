/* 
Sets view distance based on GUI selection.

Params:
None

Return value:
None

Example:
[2000] call AF_GUI_setViewDistance;

Locality:
Local effect.
*/
private _dist = (_this select 0);
private _dist2 = _dist * 0.7;
if (_dist > 500) then
{
	if (_dist < maxviewdistance) then
	{
		setViewDistance _dist;
		if (_dist2 < 500) then
		{
		setobjectviewdistance 500;
		}
		else
		{
		setobjectviewdistance _dist2;
		};
	}
	else {setViewDistance maxviewdistance; setobjectviewdistance maxviewdistance * 0.7;
	};
}
else
{
	setViewDistance 500;
	setobjectviewdistance 500;
};
ctrlSetText [311, str viewdistance];