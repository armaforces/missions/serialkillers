/* 
Ends game with SideScore. Sets winning side.

Params:
0: <SIDE> - Side which won

Return value:
None

Example:
[WEST] call MDL_SK_fnc_end;

Locality:
Runs on server only.
*/

params ["_side"];

private _score = scoreSide _side;
private _winners = "";
if (_score < 0) then {
	_score = _score + 2137;
}
else {
	_score = 2137 - _score;
};

_side addScoreSide _score;
"SideScore" call BIS_fnc_endMissionServer;
["Sound_of_Da_Police",26] spawn MDL_fnc_playMusic;
if (_side == WEST) then {
	_winners = "Policjanci";
	[[WEST, "HQ"],"US_1mc_mission_success_01"] remoteExec ["sideRadio",0];
	private _radio = selectRandom ["RU_1mc_mission_fail_01","RU_1mc_mission_fail_02","RU_1mc_mission_fail_03"];
	[[EAST, "HQ"],_radio] remoteExec ["sideRadio",0];
} else {
	_winners = "Terroryści";
	private _radio = selectRandom ["RU_1mc_mission_success_01","RU_1mc_mission_success_02","RU_1mc_mission_success_03"];
	[[EAST, "HQ"],_radio] remoteExec ["sideRadio",0];
	private _radio = selectRandom ["UK_1mc_mission_fail_02","US_1mc_mission_fail_03"];
	[[WEST, "HQ"],_radio] remoteExec ["sideRadio",0];
};
private _msg = format ["%1 wygrali!",_winners];
[[_msg, "PLAIN DOWN", 3]] remoteExec ["titleText",0];