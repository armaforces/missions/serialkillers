/* CIVILIAN VEHICLES ALARM
Handle vehicle alarm after anyone entered it.

Params:
0: <OBJECT> - vehicle with alarm

Return value:
None

Example:
[_vehicle] call MDL_SK_fnc_vehicle_alarm;

Locality:
Runs on server only. Effect global.
*/

params ["_vehicle"];
// 33.3% chance for an alarm going off.
if (_vehicle getVariable ["Alarm_Off",false] && (random 10) >= (2/3)) then {
	// Set variable to make sure alarm cannot trigger again for the same vehicle.
	_vehicle setVariable ["Alarm_Off",true];
	// Remove GetIn EH as it's not needed anymore.
	_vehicle removeAllEventHandlers "GetIn";
	// Get nearest town.
	private _nearest_town = [_vehicle] call MDL_SK_fnc_nearest_town;
	// Remotely exec function on all clients to show alarm message.
	[_nearest_town,_vehicle] remoteExec ["MDL_SK_fnc_vehicle_alarm_msg",0];
} else {
	// Set variable to make sure alarm cannot trigger for the same vehicle as it did not go off.
	_vehicle setVariable ["Alarm_Off",true];
	// Remove GetIn EH as it's not needed anymore.
	_vehicle removeAllEventHandlers "GetIn";
};