/* COP PRESET SELECT
Creates actions on object to allow player selecting predefined equipment.
Removes after 30 s.

Params:
0: <OBJECT> - object to assign actions to
1: <NUMBER> - index of last unlocked preset from list SK_Cop_Presets

Return value:
None

Example:
[player, 4] spawn MDL_SK_fnc_cop_preset_select_add;

Locality:
Runs local. Effect local.
*/


params ["_unit","_i"];
// 
#include "config\cop_presets.sqf";
hint "Rozpoczynam działanie";
sleep 1;
private _Ids = [];
while {_i >= 0} do {
	hint format ["%1",_i];
	sleep 1;
	_id = _unit addAction [(_SK_Cop_Presets select _i select 1), {
		_i = _this select 3 select 0;
		_SK_Cop_Presets = _this select 3 select 1;
		removeAllWeapons player;
		removeUniform player;
		removeVest player;
		removeBackpack player;
		removeHeadgear player;
		player addUniform (_SK_Cop_Presets select _i select 2 select 0);
		player addVest (_SK_Cop_Presets select _i select 2 select 1);
		player addBackpack (_SK_Cop_Presets select _i select 2 select 2);
		player addHeadgear (_SK_Cop_Presets select _i select 2 select 3);
		_BasicItems = ["ACE_morphine","ACE_morphine","ACE_epinephrine","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACRE_PRC343","ACRE_PRC152","ACE_Flashlight_XL50","ACE_MapTools","ACE_CableTie","ACE_CableTie"];
		{
			player addItem _x;
		} forEach _BasicItems;
		_Weapons = (_SK_Cop_Presets select _i select 3 select 0 select 0) + (_SK_Cop_Presets select _i select 3 select 1 select 0) + (_SK_Cop_Presets select _i select 3 select 2 select 0);
		{
			player addWeapon _x;
		} forEach _Weapons;
		_Magazines = (_SK_Cop_Presets select _i select 3 select 0 select 1) + (_SK_Cop_Presets select _i select 3 select 1 select 0) + (_SK_Cop_Presets select _i select 3 select 2 select 1);
		{
			player addMagazine _x;
		} forEach _Magazines;
		_i = _this select 3;
	}, [_i,_SK_Cop_Presets], 6, true, true, "", "true", 5];	
	_Ids pushback _id;
	_i = _i - 1;
};

// Wait 30 s so player can choose preset in mean time.
uisleep 30;
hint "Usuwam akcje";
// Remove all preset actions
{
	_unit removeAction _x;
} forEach _Ids;