diary1 = player createDiaryRecord ["Diary", ["Zasady",
 "<t underline='true' color='#ff0000'>Zasady</t><br/>
1. Policja nie korzysta ze skrzynek terrorystów, ale może używać tej broni pozostawionej przez terrorystów (również martwych) np. w pojazdach, którymi się poruszali.<br />
2. Policja nie (zabija/molestuje/okalecza) cywili nie mając 100% pewności, że to może być terrorysta.<br />
3. Policja nie zabija więźniów, jeśli nie są uzbrojeni i nie stanowią zagrożenia.<br />
4. Policja nie powinna używać więcej niż 1 helikoptera (2 maksymalnie przy dużej liczbie graczy, po 1 na pilota), a w szczególności nie używać ich jako pocisków kinetycznych.<br />
5. Terroryści nie powinni używać policyjnego arsenału ani spawnowania pojazdów.<br />
6. Terroryści powinni zapewnić na tyle akcji, żeby policja nie mogła się nudzić.<br />
7. Jeżdzenie po normalnych drogach (A NIE PO LASACH) na pewno uprzyjemni wszystkim rozgrywkę.<br />
8. Policja nie powinna kraść pojazdów cywilnych.<br />"
]]; 

diary1 = player createDiaryRecord ["Diary", ["Progi odblokowania",
 " <br/>
 Broń poboczna: <br/>
 [0] - M9 <br/>
 [0] - Glock <br/>
 [10] - FNP z tarczą <br/>
 [25] - MP7A1 <br/>
 [40] - M320 GLM <br/>
 <br/>
 Broń podstawowa: <br/>
 [10] - Shotgun M590 <br/>
 [15] - MP5 <br/>
 [20] - MP5SD <br/>
 [20] - M16A4 <br/>
 [20] - SPAS-12 <br/>
 [25] - M79 <br/>
 [30] - M4A1 (M203)<br/>
 [30] - M249 <br/>
 [35] - M32 <br/>
 <br/>
 Wyrzutnie: <br/>
 [35] - M136 <br/>
 <br/>
 Granaty: <br/>
 [10] - Biały dym <br/>
 [20] - Flashbang (ręczny i do granatników) <br/>
 [30] - Czerwony dym <br/>
 [30] - Granat odłamkowy <br/>
 [40] - M441 HE (M203 i M32) <br/>
 <br/>
 Celowniki: <br/>
 [15] - ACO SMG <br/>
 [20] - ACO <br/>
 [30] - Eotech 552 <br/>
 <br/>
 Pojazdy: <br/>
 [0] - Czarna Wołga <br/>
 [0] - Land Rover (policyjny) <br/>
 [0] - Offroad (policyjny) <br/>
 [15] - Hatchback (sportowy) <br/>
 [20] - HMMWV (Nieuzbrojony) <br/>
 [20] - UAZ (DShKM) <br/>
 [20] - Offroad (M2) <br/>
 [30] - HMMWV (M2) <br/>
 [35] - BTR-80A <br/>
 [40] - GAZ Tigr <br/>
 <br/>
 Pojazdy latające: <br/>
 [0] - UH-1H (Nieuzbrojony) <br/>
 [0] - Wildcat (Szperacz) <br/>
 [10] - UH-60M (Nieuzbrojony) <br/>
 [20] - UH-1H (M240) <br/>
 [20] - MH-6M <br/>
 [30] - YABHON-R3 <br/>
 [40] - AH-1Z <br/>
 "
 ]];
 
diary1 = player createDiaryRecord ["Diary", ["Informacja dla Policji",
 "Policjanci mogą się szybko przemieszczać pomiędzy posterunkami wybierając nazwę posterunku z menu pod kółkiem przy fladze na posterunku. Oprócz tego na posterunkach można tworzyć nowe pojazdy ponownie z menu pod kółkiem przy skrzynce/pudle.<br/><br/>
 Na głównym posterunku policja może wybrać sobie broń i wyposażenie. Im więcej cywili zostanie wyeliminowanych, tym więcej będzie dostępnego sprzętu dla Policji (również pojazdów!).<br/><br/>"
 ]]; 

diary1 = player createDiaryRecord ["Diary", ["Informacja dla Terrorystów",
 "Jako terrorysta zaczynasz w 'bazie' na północy mapy. Po przygotowaniu ekwipunku możesz podejść do flagi i teleportować się na pozycję startową. Teraz możesz rozpocząć eliminację cywili. Spróbuj zabijać ich szybko, by Policja się nie nudziła. Jeśli zostaniesz zabity pojawisz się w więzieniu goły i będziesz miał tryb obserwatora. Jeśli jest więcej niż 1 terrorysta to można uwolnić więźnia (w momencie uwolnienia tryb obserwatora zostaje wyłączony)."
 ]]; 
 
diary1 = player createDiaryRecord ["Diary", ["Informacje ogólne",
 "Każdy cywil jest oznaczony na mapie zieloną kropką. Jeśli zostanie zabity to będzie oznaczony czerwonym krzyżykiem w kółku wraz z godziną o której to się stało. Cywil zabity przez policję tylko pomoże terrorystom!"
 ]]; 
 
diary1 = player createDiaryRecord ["Diary",
 ["Koncepcja", "Misja jest przeznaczona dla 2 terrorystów (ale jak jest mnóstwo policjantów, to może być i 4) przeciwko 24 policjantom.<br/><br/>Celem terrorystów jest wyeliminowanie wszystkich cywili na Czarnorusi (a tak naprawdę zdobycie limitu [50] punktów; 1 za każdego martwego cywila, 2 za policjanta NAWET JAK SIĘ SAM ZABIJE!). Zadaniem policji jest unieszkodliwić terrorystów zanim tego dokonają.<br/>Za każdym razem jak cywil zostanie zabity, policja dostaje informację gdzie to nastąpiło. Im więcej zostanie zabitych cywili, tym lepszy ekwipunek otrzyma policja.<br/><br/>Policja wygrywa, gdy wszyscy terroryści są w więzieniu. Terroryści wygrywają, gdy osiągną limit punktów [50]."
 ]];

diary1 = player createDiaryRecord ["Diary", ["O misji",
 "Misja pierwotnie utworzona przez NeoArmageddon i Mondkalb.<br/><br/>
 Zrobiona od nowa w Armie 3 przez 3Mydlo3. <br/><br/>
 Odwiedź www.armed-tactics.de po więcej ich misji.<br/><br/>
 Nie zachęcamy do seryjnych zabójstw i morderstw. Jeśli regularnie zabijasz niewinnych ludzi, albo ostatnio Ci się zdarzyło, NIE GRAJ w to. Ta misja nie może być używana jako symulator seryjnych zabójstw. Jeśli wydaje Ci się inaczej, to jesteś po prostu debilem.<br/><br/>"
 ]]; 