SK_Arsenals = [];
SK_BLUFOR_Score = 0;
SK_BLUFOR_Score_Max = 40;
SK_Cities = [];
SK_Civ_Killed = false;
SK_Civis = [];
SK_Cop_bases = [];
SK_Cops = [];
SK_Markers=[];
SK_OPFOR_Score = 0;
SK_OPFOR_Score_Max = 50;
SK_serverInitialized = false;
SK_serverVariables = false;
SK_snow = false;
SK_Terro = [killer1, killer2, killer3, killer4];
SK_Terro_ammoCrates = [];
SK_Terro_count = 0;
SK_Terro_jail_count = 0;
SK_Terro_Hide_Markers = [];
SK_Terro_spawns = [];
SK_Terro_spawns_markers = [];
// Random civilian vehicles
SK_Civilian_Vehicles = [
	"C_Van_01_transport_F",
	"C_Van_01_fuel_F",
	"C_Van_01_box_F",
	"C_Hatchback_01_F",
	"C_Hatchback_01_sport_F",
	"C_Truck_02_fuel_F",
	"C_Truck_02_box_F",
	"C_Truck_02_transport_F",
	"C_Truck_02_covered_F",
	"C_Quadbike_01_F",
	"C_SUV_01_F",
	"C_Offroad_01_F",
	"C_Offroad_01_repair_F",
	"LOP_CHR_Civ_Landrover",
	"LOP_CHR_Civ_UAZ",
	"LOP_CHR_Civ_UAZ_Open",
	"LOP_CHR_Civ_Ural",
	"LOP_CHR_Civ_Ural_Open",
	"RDS_Gaz24_Civ_03",
	"RDS_Gaz24_Civ_02",
	"RDS_Gaz24_Civ_01",
	"RDS_Golf4_Civ_01",
	"RDS_Ikarus_Civ_01",
	"RDS_Ikarus_Civ_02",
	"RDS_JAWA353_Civ_01",
	"RDS_MMT_Civ_01",
	"RDS_Old_bike_Civ_01",
	"RDS_S1203_Civ_01",
	"RDS_S1203_Civ_02",
	"RDS_S1203_Civ_03",
//	"RDS_Octavia_Civ_01",
	"RDS_Zetor6945_Base",
	"RDS_Lada_Civ_01",
	"RDS_Lada_Civ_02",
	"RDS_Lada_Civ_03",
	"RDS_Lada_Civ_04",
	"RDS_tt650_Civ_01"
];
// POLICE SPAWN MENU VEHICLES [Score Needed, Name displayed, Classname]
SK_Vehicles = [
//	[0,"Rower","RDS_MMT_Civ_01"],
//	[0,"Łada (Milicja)","RDS_Lada_Civ_05"],
//	[0,"Quad","B_Quadbike_01_F"],
	[0,"Wołga (Czarna)","RDS_Gaz24_Civ_03"],
//	[5,"Skoda Octavia II 2.0 TDI","RDS_Octavia_Civ_01"],
	[0,"Land Rover (Policyjny)","UK3CB_BAF_LandRover_Hard_FFR_RAF_A"],
	[0,"Offroad (Policyjny)","B_GEN_Offroad_01_gen_F"],
//	[5,"UAZ","LOP_CDF_UAZ_Open"],
	[15,"Hatchback (sportowy)","C_Hatchback_01_sport_F"],
	[20,"HMMWV (Nieuzbrojony)","rhsusf_m1025_w"],
	[20,"UAZ (DShKM)","LOP_CDF_UAZ_DshKM"],
	[20,"Offroad (M2)","LOP_CDF_Offroad_M2"],
	[30,"HMMWV (M2)","rhsusf_m1025_w_m2"],
	[35,"BTR-80A","rhs_btr80a_vdv"],
//	[35,"M2A3 Bradley","RHS_M2A3_wd"],
	[40,"GAZ Tigr","rhs_tigr_sts_msv"],
//	[40,"M1A2","rhsusf_m1a2sep1wd_usarmy"]
	[0,""]
];
// POLICE SPAWN MENU HELICOPTERS [Score Needed, Name displayed, Classname]
SK_Helis = [
	[0,"UH-1H (Nieuzbrojony)","rhs_uh1h_hidf_unarmed"],
//	[0,"Wildcat (Szperacz)","UK3CB_BAF_Wildcat_AH1_TRN_8A"],
	[10,"UH-60M (Nieuzbrojony)","RHS_UH60M2"],
	[20,"UH-1H (M240)","rhs_uh1h_hidf"],
	[20,"MH-6M","RHS_MELB_MH6M"],
//	[30,"UH-60M","RHS_UH60M"],
	[40,"AH-1Z","RHS_AH1Z"]
];
// POLICE ARSENAL WEAPONS [Score Needed, [Item Classname]]
SK_PoliceWeapons = [
	[10,["KA_FNP45","KA_15Rnd_45ACP_Mag","ka_fnp45_shield2"]],
	[10,["rhs_weap_M590_8RD","rhsusf_8Rnd_00Buck"]],
	[10,["rhs_mag_an_m8hc","rhs_mag_m714_White","rhsusf_mag_6Rnd_M714_white"]], //Biały dym
	[15,["hlc_smg_mp5a4","hlc_30Rnd_9x19_B_MP5"]],
	[15,["optic_aco_grn_smg","optic_aco_smg"]],
	[20,["rhs_mag_mk84","rhs_mag_m4009","rhsusf_mag_6Rnd_m4009"]], //Flashbang
	[20,["KA_SPAS12","8Rnd_SPAS12_buck","8Rnd_SPAS12_slug"]],
	[20,["hlc_smg_mp5sd6","hlc_30Rnd_9x19_SD_MP5"]],
	[20,["rhs_weap_m16a4_carryhandle","rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
	[20,["optic_aco_grn","optic_aco"]],
	[25,["rhs_weap_m79"]],
	[25,["KA_MP7_Pistol_Black_20Rnd","KA_20Rnd_46x30_FMJ"]],
	[30,["rhs_weap_m249_pip_L","rhs_200rnd_556x45_M_SAW"]],
	[30,["rhsusf_acc_eotech_552"]],
	[30,["rhs_weap_m4a1_carryhandle_m203"]],
	[30,["rhs_mag_m18_red","rhs_mag_m713_Red","rhsusf_mag_6Rnd_M713_red"]], //Czerwony dym
	[30,["rhs_mag_m67"]], //Granat odłamkowy
	[35,["rhs_weap_m32"]], //Granatnik rewolwerowy
	[35,["rhs_weap_M136_hedp","rhs_m136_hedp_mag"]],
	[40,["rhs_mag_M441_HE","rhsusf_mag_6Rnd_M441_HE"]], //Wybuchowe do granatników
	[40,["rhs_weap_M320"]] //Wybuchowe do granatników
];

// TERRO SPECIAL AMMO CRATE SHIT [[Weapon count, Weapon classname],[[Magazine count, Magazine classname],[Item count, Classname]]]

SK_SpecialClothesCivilian = [
	//Sets
	//Uniform | Vest | Backpack | Headgear
	//Woodlander sets
	[[1,"LOP_U_CHR_Woodlander_01"],[[1,""],[1,""],[1,"rds_Woodlander_cap3"]]],
	[[1,"LOP_U_CHR_Woodlander_02"],[[1,""],[1,""],[1,"rds_Woodlander_cap2"]]],
	[[1,"LOP_U_CHR_Woodlander_03"],[[1,""],[1,""],[1,"rds_Woodlander_cap1"]]],
	[[1,"LOP_U_CHR_Woodlander_04"],[[1,""],[1,""],[1,"rds_Woodlander_cap4"]]],

	//Backpacks
	//2x Field pack
	[[0,""],[[2,"B_FieldPack_cbr"]]],
	[[0,""],[[2,"B_FieldPack_khk"]]],
	[[0,""],[[2,"B_FieldPack_oli"]]],
	//1x Carryall
	[[0,""],[[1,"B_Carryall_cbr"]]],
	[[0,""],[[1,"B_Carryall_oli"]]],
	//1x Kitbag
	[[0,""],[[1,"B_Kitbag_cbr"]]],
	[[0,""],[[1,"B_Kitbag_rgr"]]],
	[[0,""],[[1,"B_Kitbag_tan"]]],

	//Vests
	//Small carriage
	[[0,""],[[4,"rds_pistol_holster"]]],
	[[0,""],[[4,"rhs_vest_pistol_holster"]]],
	[[0,""],[[4,"rhs_vest_commander"]]],
	[[0,""],[[4,"rhs_6sh46"]]],
	[[0,""],[[2,"V_BandollierB_blk"]]],
	[[0,""],[[2,"V_BandollierB_khk"]]],
	[[0,""],[[2,"V_BandollierB_oli"]]],
	[[0,""],[[2,"V_BandollierB_rgr"]]],
	
	[[0,""],[[0,""]]]
];

SK_SpecialClothesMilitary = [
	// Uniform | Vest | Backpack | Headgear
	[[1,"rhssaf_uniform_m93_oakleaf"],[[1,"rhssaf_vest_md99_woodland_rifleman_radio"],[1,""],[1,"rhssaf_helmet_m97_oakleaf"]]],
	[[1,"rhssaf_uniform_m93_oakleaf"],[[1,"rhssaf_vest_md99_woodland_rifleman"],[1,"rhssaf_kitbag_md2camo"],[1,"rhssaf_helmet_m97_oakleaf"]]],
	[[1,"rhssaf_uniform_m93_oakleaf"],[[1,"rhsgref_alice_webbing"],[1,"rhssaf_alice_md2camo"],[1,"rhssaf_helmet_m97_oakleaf"]]],
	[[0,""],[[0,""],[0,""],[0,""]]]
];

SK_SpecialItems = [
//	[[0,""],[[1,"ACRE_PRC148"]]],
	[[0,""],[[1,"ACRE_SEM52SL"]]],
	[[0,""],[[5,"ACE_elasticBandage"],[2,"ACE_morphine"],[1,"ACE_epinephrine"]]],
	[[0,""],[[5,"ACE_elasticBandage"],[2,"ACE_morphine"],[1,"ACE_epinephrine"]]],
	[[0,""],[[5,"ACE_elasticBandage"],[2,"ACE_morphine"],[1,"ACE_epinephrine"]]],
	[[0,""],[[4,"rhs_mag_m67"],[2,"rhs_mag_an_m8hc"],[1,"rhs_mag_an_m14_th3"]]],
	[[0,""],[[3,"rhs_mag_rgo"],[2,"rhs_mag_rdg2_black"],[2,"rhs_mag_rgn"]]],
	[[0,""],[[2,"DemoCharge_Remote_Mag"]]],
	[[0,""],[[1,"IEDUrbanBig_Remote_Mag"]]],
	[[0,""],[[3,"IEDUrbanSmall_Remote_Mag"]]],
	[[0,""],[[1,"SLAMDirectionalMine_Wire_Mag"]]],
	[[0,""],[[1,"ClaymoreDirectionalMine_Remote_Mag"]]],
	[[0,""],[[1,"ACE_M26_Clacker"]]],
	[[0,""],[[1,"O_UavTerminal"],[1,"O_UAV_01_backpack_F"]]],
	[[0,""],[[0,""]]]
];

SK_SpecialLaunchers = [
	[[1,"ATA_XM25_F"],[[1,"ATA_6Rnd_HE_25mm"]]],
	[[1,"rhs_weap_m32"],[[2,"rhsusf_mag_6Rnd_M441_HE"]]],
	[[1,"rhs_weap_fgm148"],[[1,"rhs_fgm148_magazine_AT"]]],
	[[1,"rhs_weap_fim92"],[[1,"rhs_fim92_mag"]]],
	[[1,"rhs_weap_maaws"],[[2,"rhs_mag_maaws_HE"]]],
	[[2,"rhs_weap_m80"],[[0,""]]],
	[[2,"rhs_weap_rshg2"],[[0,""]]],
	[[0,""],[[0,""]]]
];

SK_SpecialPistols = [
	[[1,"Desert_Eagle"],[[4,"7Rnd_50_AE"]]],
	[[1,"rhs_weap_pp2000_folded"],[[3,"rhs_mag_9x19mm_7n21_44"]]],
	[[1,"rhs_weap_pp2000_folded"],[[5,"rhs_mag_9x19mm_7n21_420"]]],
	[[1,"KA_FN57"],[[3,"KA_20Rnd_57x28_SS190"]]],
	[[1,"KA_FNP45"],[[4,"KA_15Rnd_45ACP_Mag"]]],
	[[1,"hlc_smg_mp5k"],[[3,"hlc_30Rnd_9x19_GD_MP5"]]],
	[[0,""],[[0,""]]]
];

SK_SpecialRifles = [
	[[1,"KA_M16A2"],[[5,"30Rnd_556x45_Stanag_red"]]],
	[[1,"rhs_weap_akm"],[[4,"rhs_30Rnd_762x39mm_bakelite"]]],
	[[1,"hlc_rifle_g3a3"],[[5,"hlc_20rnd_762x51_b_G3"]]],
	[[1,"hlc_rifle_FAL5000"],[[5,"hlc_20Rnd_762x51_B_fal"]]],
	[[1,"KA_UMP45"],[[5,"KA_25Rnd_45ACP_FMJ_Mag"],[1,"optic_holosifht_smg_blk_f"]]],
	[[1,"rhs_weap_m21a"],[[5,"rhsgref_30rnd_556x45_m21"],[1,"rhs_acc_pkas"]]],
	[[0,""],[[0,""]]]
];

SK_SpecialWeapons = [
	[[1,"AA12"],[[2,"20Rnd_AA12_Pellets"],[1,"20Rnd_AA12_HE"],[2,"20Rnd_AA12_74Slug"]]],
	[[1,"KA_NTW20"],[[1,"KA_NTW20_3rnd_MK106_HEI"],[1,"ka_ntw20_scope"]]],
	[[1,"rhs_weap_m249_pip_L"],[[2,"rhsusf_200Rnd_556x45_box"],[1,"rhsusf_acc_eotech_552"]]],
	[[1,"KA_FNP45"],[[3,"KA_15Rnd_45ACP_Mag"],[1,"ka_fnp45_shield"]]],
	[[1,"rhs_weap_M107"],[[3,"rhsusf_mag_10Rnd_STD_50BMG_M33"],[1,"rhsusf_acc_leupoldmk4_2"],[1,"rhsusf_acc_eotech_552"]]],
	[[1,"rhs_weap_M107"],[[2,"rhsusf_mag_10Rnd_STD_50BMG_mk211"],[1,"rhsusf_acc_leupoldmk4_2"],[1,"rhsusf_acc_eotech_552"]]],
	[[0,""],[[0,""]]]
];


SK_TerroWeapons = [
	[[1,"hlc_lmg_m60"],[[4,"hlc_100Rnd_762x51_M_M60E4"]]],
	[[1,"rhs_weap_pm63"],[[3,"rhs_75Rnd_762x39mm_tracer"]]],
	[[1,"hlc_rifle_g3a3"],[[2,"hlc_20rnd_762x51_b_G3"],[1,"hlc_optic_zfsg1"],[1,"hlc_muzzle_300blk_kac"]]],
	[[3,"KA_UMP45"],[[12,"KA_25Rnd_45ACP_FMJ_Mag"]]],
	[[4,"hlc_rifle_FAL5000"],[[16,"hlc_20Rnd_762x51_B_fal"]]],
	[[4,"rhs_weap_panzerfaust60"],[[0,""]]],
	[[1,"rhs_weap_rpg7"],[[1,"rhs_rpg7_PG7V_mag"],[1,"rhs_rpg7_OG7V_mag"],[1,"rhs_rpg7_TBG7V_mag"]]],
	[[1,"Desert_Eagle"],[[2,"7Rnd_50_AE"]]],
	[[3,"rhs_weap_pp2000_folded"],[[6,"rhs_mag_9x19mm_7n21_44"]]],
	[[0,""],[[1,"SLAMDirectionalMine_Wire_Mag"],[2,"DemoCharge_Remote_Mag"]]],
	[[0,""],[[0,""]]]
];

SK_civilian_markers_initialized = false;
SK_players_markers_initialized = false;
SK_music_playing = false;
SK_music_on = true;
SK_radio_on = true;
SK_radio_terro_losing = false;

//cop_base variables
boxes_parent_class = "CargoNet_01_base_F";
heli_spawn_location = "HeliH";
heli_spawn_point = "CargoNet_01_barrels_F";
vehicle_spawn_location = "Land_HelipadCivil_F";
vehicle_spawn_point = "CargoNet_01_box_F";
arsenal_box = "B_CargoNet_01_ammo_F";
teleport_flag = "Flag_US_F";

//terro spawn variables
teleport_flag_terro = "rhssaf_flag_serbia";
delete_terro_base = false;



terroTeleportFlag = "";
CopTeleportFlags = [];
CopSpawners = [];


maxviewdistance = "MaxViewDistance" call BIS_fnc_getParamValue;
defaultviewdistance = 2000;