while {true} do {
	if (SK_OPFOR_Score >= 50) exitwith {
		[EAST] call MDL_SK_fnc_end;
	};
	_terro_status = call MDL_SK_fnc_terro_check;
	sleep 1;
	if (_terro_status) then {
		sleep 10;
		_terro_status = call MDL_SK_fnc_terro_check;
		sleep 1;
		if (_terro_status) exitwith {
			[WEST] call MDL_SK_fnc_end;
		};
	};
	sleep 5;
};