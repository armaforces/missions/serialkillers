maxviewdistance = "MaxViewDistance" call BIS_fnc_getParamValue;
defaultviewdistance = 2000;

//player addweapon "itemgps";
//player addweapon "Binocular_Vector";

_loadout = + getUnitLoadout player;
player setVariable ["Saved_Loadout",_loadout];

_side = side player;
player setVariable ["Player_Side",_side];

if(_side == WEST) then {
	SK_Cops pushback player;
	publicVariable "SK_Cops";
};

sleep 5;

/*
onKeyPress = compile preprocessFile "client\OnKeyPress.sqf";
waituntil {!(IsNull (findDisplay 46))};
(findDisplay 46) displaySetEventHandler ["KeyDown", "_this call onKeyPress"];
*/

//waituntil {ScriptDone _ScrMarkerHandler};
//_ScrMarkerHandler = [] execVM "client\markers_client.sqf";

_id = clientOwner;
[_id,"SK_serverInitialized"] remoteExec ["publicVariableClient",2];
systemChat "Requesting server status";
waitUntil {SK_serverInitialized};
systemChat "Server initialized";
[_id] remoteExec ["MDL_SK_fnc_serverVariables",2];

waitUntil {SK_serverVariables};
systemChat "Server variables initialized";
SK_serverVariables = false;
publicVariableServer "SK_serverVariables";


call MDL_SK_fnc_terro_teleport_client;
{
	[_x] call MDL_SK_fnc_cop_base_teleport;
} forEach CopTeleportFlags;
systemChat "Teleports created";

{
	_x addAction ["Zapisz ekwipunek do respawnu",{_loadout = + getUnitLoadout player; player setVariable ["Saved_Loadout",_loadout];}];
} forEach SK_Arsenals;

//EH to check if player has UAV terminal and reassign it to player after closing arsenal
["ace_arsenal_displayOpened", {
	if (assignedItems player findIf {_x == "B_UavTerminal"} != -1) then {
		player setVariable ["SK_UAVTerminal",true];
	} else {
		player setVariable ["SK_UAVTerminal",false];
	};
}] call CBA_fnc_addEventHandler;
["ace_arsenal_displayClosed", {
	if (player getVariable ["SK_UAVTerminal",false]) then {
		player unassignItem "ItemGPS";
		player removeItem "ItemGPS";
		player addItem "B_UavTerminal";
		player assignItem "B_UavTerminal";
	};
}] call CBA_fnc_addEventHandler;

//Initialize cop spawners
{
if(typeOf _x == heli_spawn_point) then {
	_x addAction ["Odśwież menu helikopterów", {
		[_this select 0] spawn MDL_SK_fnc_spawner_reload;
	}, {}, 6, true, true, "", "true", 2];
} else {
	_x addAction ["Odśwież menu pojazdów", {
		[_this select 0] spawn MDL_SK_fnc_spawner_reload;
	}, {}, 6, true, true, "", "true", 2];
};
} forEach CopSpawners;
systemChat "Spawners created";

execVM "client\markers_client.sqf";
execVM "client\markers_client_civs.sqf";
systemChat "Markers online";

if (SK_snow) then {
	null = [80,3600,false,40,false,false,false,true,false,true] execvm "AL_snowstorm\al_snow.sqf";
	maxviewdistance = 200;
	systemChat "Let it snow, let it snow, let it snow!";
	[] spawn {
		sleep 15;
		setViewDistance 200;
	};
};

if (side player == EAST) then {
	publicVariable "SK_Terro";
};

player addAction ["<t color='#c8c8ff'>Ustaw odległość widzenia</t>", {call AF_GUI_setViewDistanceGUI},nil, -10, false, true, "", "", 50];
systemChat "ViewDistance GUI initialized";

execVM "client\ragdolls.sqf";
//systemChat "Ragdolls online";