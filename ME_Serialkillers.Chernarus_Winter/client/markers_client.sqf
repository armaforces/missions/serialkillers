if (SK_players_markers_initialized) exitwith {};
SK_players_markers_initialized = true;

if (side player==West) then {
	while {true} do {
		_Markers=[];
		{
			if (side _x == WEST AND alive _x) then {
				_name = format["copmarker%1",name _x];
				_marker = createMarkerlocal [_name, getpos _x];
				_marker setmarkertypelocal "mil_dot";
				_marker setmarkercolorlocal "ColorBlue";
				if (vehicle _x == _x) then {
					_marker setmarkersizelocal [0.5,0.5];
				} else {
					_marker setmarkersizelocal [0.75,0.75];
				};
				if (player == _x) then {
					_marker setmarkercolorlocal "ColorYellow";
				};
				_marker setmarkertextlocal name _x;
				_Markers pushback [_marker,_x];
			};
		} forEach AllPlayers;
		sleep 1;
		
		
		{
			_marker = _x select 0;
			deletemarkerlocal _marker;
		} forEach _Markers;
		
	};
};

if (side player==East) then {
	while {true} do {
		_Markers=[];
		{
			if (!isNil("_x")) then {
				_terro = _x;
				_name = format ["%1_marker",name _terro];
				_marker = createMarkerlocal [_name, getpos _terro];
				_marker setmarkertypelocal "mil_dot";
				_marker setmarkercolorlocal "ColorRed";
				_marker setmarkertextlocal name _terro;
				_Markers pushback [_marker,_terro];
				{
					_hide_marker_name = _x;
					_terro_marker_name = format ["%1_hide_marker",name _terro];
					if (_hide_marker_name == _terro_marker_name) exitwith {
					_marker setmarkercolorlocal "ColorGreen";
					};
				} forEach SK_Terro_Hide_Markers;
			};
		} forEach SK_Terro;
		sleep 1;	
		{
			_marker = _x select 0;
			deletemarkerlocal _marker;
		} foreach _Markers;
	};
};

