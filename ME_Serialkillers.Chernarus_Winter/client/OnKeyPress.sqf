private["_handled"];

_key     = _this select 1;
_shift   = _this select 2;
_handled = false;

switch _key do

{
	//Left Shift key
	case 42: 

	{
		_vcl = vehicle player;
	
		if(_vcl == player)exitwith{};
		
		if(
		//_vcl isKindOf "Car" AND 
		isEngineOn _vcl) then
	
			{
			_boost = 1.005;
			_fuelused = 0.00005;
			_vel  = velocity _vcl;
			_spd  = speed _vcl;
			_fuel = fuel _vcl;
			_vcl setVelocity [(_vel select 0) * _boost, (_vel select 1) * _boost, (_vel select 2)];
			_vcl setfuel (_fuel - _fuelused);
			
			};
	};
	
	//Left Shift key
	case 207: 
	{
		_vcl = vehicle player;
	
		if(_vcl == player)exitwith{};
		
		if(isEngineOn _vcl) exitwith {player action ["engineOff",_vcl];};
		if(!(isEngineOn _vcl)) exitwith {player action ["engineOn",_vcl];};
	};
	//E key
	case 18: 
	
	{

	if(vehicle player == player) exitwith

		{

		private ["_vcl"];

		for [{_i=1}, {_i < 3}, {_i=_i+1}] do
    
			{

       			_range = _i;
       			_dirV = vectorDir vehicle player;
       			_pos = player modelToWorld [0,0,0];
        		_posFind = [(_pos select 0)+(_dirV select 0)*_range,(_pos select 1)+(_dirV select 1)*_range,(_pos select 2)+(_dirV select 2)*_range];
       	 		_vcls    = nearestobjects [_posFind,["LandVehicle", "Air", "ship"], 2];
				_vcl     = _vcls select 0;

			if(!(isnull _vcl))exitwith{_i = 4};

			}; 

		//if(locked _vcl)exitwith{};
		
		if(_vcl emptyPositions "Driver" > 0)exitwith   {player action ["getInDriver", _vcl]};
		if(_vcl emptyPositions "Gunner" > 0)exitwith   {player action ["getInGunner", _vcl]};
		if(_vcl emptyPositions "Commander" > 0)exitwith{player action ["getInCommander", _vcl]};
		if(_vcl emptyPositions "Cargo" > 0)exitwith    {player action ["getInDriver", _vcl];_vcl spawn {keyblock=true;sleep 0.5;player moveincargo _this; keyblock=false;};};
			
		};

	_vcl  = vehicle player;

	if(_vcl != player) exitwith

		{
		if(speed _vcl > 30)exitwith{player groupchat "Chcesz się zabić?!"};
		if(speed _vcl > 2)exitwith{player action ["Eject", _vcl];};
		player action ["getOut", _vcl];
		};

	};
};

_handled;



