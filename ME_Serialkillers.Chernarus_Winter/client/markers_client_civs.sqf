// Script by NeoArmageddon
// DO NOT spread,release,copy and modify this script without permission of the author
// www.armed-tactics.de

if (SK_civilian_markers_initialized) exitwith {};
SK_civilian_markers_initialized = true;

{
	_name = format ["marker%1",(random 9999)];
	_marker = createMarkerlocal [_name, getpos _x];
	_marker setmarkertypelocal "mil_dot";
	_marker setmarkercolorlocal "ColorGreen";
	_marker setmarkersizelocal [0.6,0.6];
	SK_Markers pushback [_marker,_x];
} forEach SK_Civis;

_terro_concealed_weapons = [
	"Binocular",
	"LaserDesignator",
	"rhsusf_weap_m9",
	"rhsusf_weap_glock17g4",
	"hlc_smg_mp5k",
	"KA_MP7_Pistol_Black_20Rnd",
	"Desert_Eagle",
	"rhs_weap_pp2000_folded",
	"KA_FN57",
	"KA_FNP45",
	"rhs_weap_M320"
];

while {true} do {
	{
		if(!isNil("_x")) then {
			_terro = _x;
			_wc=0;
			{
				if (_terro hasweapon _x) exitwith {
					_wc = _wc + 1;
				};
			} forEach _terro_concealed_weapons;
			if ((count(weapons _terro)<=_wc) AND alive _terro) then {
				_name = format ["%1_hide_marker",name _terro];;
				_marker = createMarkerlocal [_name, getpos _terro];
				_marker setmarkertypelocal "hd_dot";
				_marker setmarkercolorlocal "ColorGreen";
				_marker setmarkersizelocal [0.6,0.6];
				SK_Terro_Hide_Markers pushback _marker;
			};
		};
	} forEach SK_Terro;

	sleep 2;
	{
		if (alive (_x select 1)) then {
			(_x select 0) setmarkerposlocal getpos (_x select 1);
		} else {
			SK_Markers = SK_Markers - [_x];
			_marker = _x select 0;
			deletemarkerlocal (_marker);
		};
	} forEach SK_Markers;
	
	{
		SK_Terro_Hide_Markers = SK_Terro_Hide_Markers - [_x];
		deletemarkerlocal _x;
	} forEach SK_Terro_Hide_Markers;
};