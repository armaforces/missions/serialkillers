sleep 5;

_base_location = getpos (_this select 0);
_base_name = _this select 1;
_base_helipad = _this select 2;
_Base = [_base_location,_base_name,_base_helipad];
SK_Cop_bases pushback _Base;
_Helipads = nearestObjects [_base_location, [heli_spawn_location], 250];
{
	_Boxes = nearestObjects [(getpos _x), [boxes_parent_class],250];
		if (typeOf _x == vehicle_spawn_location) then {
			_spawner = (_Boxes select {typeOf _x == vehicle_spawn_point}) select 0;
			CopSpawners pushback _spawner;
//			_spawner addAction ["Odśwież menu pojazdów", {
//				[_this select 0] spawn MDL_SK_fnc_spawner_reload;
//			}, {}, 6, true, true, "", "true", 2];
		};
		if (typeOf _x == heli_spawn_location) then {
			_spawner = (_Boxes select {typeOf _x == heli_spawn_point}) select 0;
			CopSpawners pushback _spawner;
//			_spawner addAction ["Odśwież menu helikopterów", {
//				[_this select 0] spawn MDL_SK_fnc_spawner_reload;
//			}, {}, 6, true, true, "", "true", 2];
		};
} forEach _Helipads;


_Arsenals = nearestObjects [_base_location, [arsenal_box],250];
{
	//_x addAction ["Zapisz ekwipunek do respawnu",{_loadout = getUnitLoadout player; player setVariable ["Saved_Loadout",_loadout];}];
	SK_Arsenals pushback _x;
} forEach _Arsenals;

sleep 5;

_marker_name = format["cop_base_%1",_base_name];
_marker_text = format["%1",_base_name];
_marker = createMarker [_marker_name, _base_location];
_marker setmarkertype "mil_flag";
_marker setmarkercolor "ColorWEST";
_marker setmarkertext _marker_text;

//_flag = "";
if (isServer) then {
_flag = teleport_flag createVehicle _base_location;
CopTeleportFlags pushback _flag;
};

/*
if (hasInterface) then {
sleep 5;
_Flags = nearestObjects [_base_location, [teleport_flag], 250];
_flag = _Flags select 0;
[_flag] call MDL_SK_fnc_cop_base_teleport;
};