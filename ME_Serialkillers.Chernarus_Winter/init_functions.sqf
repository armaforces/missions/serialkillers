AF_GUI_setViewDistance = compile preprocessFileLineNumbers "fnc\AF_GUI_setViewDistance.sqf";
AF_GUI_setViewDistanceGUI = compile preprocessFileLineNumbers "fnc\AF_GUI_setViewDistanceGUI.sqf";
MDL_fnc_playMusic = compile preprocessFileLineNumbers "fnc\MDL_fnc_playMusic.sqf";
MDL_SK_fnc_arsenal = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_arsenal.sqf";
MDL_SK_fnc_arsenal_add_items = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_arsenal_add_items.sqf";
MDL_SK_fnc_civ_killed = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_civ_killed.sqf";
MDL_SK_fnc_civ_killed_marker = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_civ_killed_marker.sqf";
MDL_SK_fnc_civ_killed_msg = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_civ_killed_msg.sqf";
MDL_SK_fnc_civs = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_civs.sqf";
MDL_SK_fnc_cop_base_alarm_msg = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_cop_base_alarm_msg.sqf";
MDL_SK_fnc_cop_base_teleport = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_cop_base_teleport.sqf";
MDL_SK_fnc_cop_killed = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_cop_killed.sqf";
MDL_SK_fnc_cop_killed_marker = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_cop_killed_marker.sqf";
MDL_SK_fnc_cop_killed_msg = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_cop_killed_msg.sqf";
MDL_SK_fnc_cop_preset_select_add = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_cop_preset_select_add.sqf";
MDL_SK_fnc_cop_preset_select_check = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_cop_preset_select_check.sqf";
MDL_SK_fnc_end = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_end.sqf";
MDL_SK_fnc_jail_free = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_jail_free.sqf";
MDL_SK_fnc_music = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_music.sqf";
MDL_SK_fnc_nearest_town = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_nearest_town.sqf";
MDL_SK_fnc_radio = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_radio.sqf";
MDL_SK_fnc_score_change = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_score_change.sqf";
MDL_SK_fnc_score_watch = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_score_watch.sqf";
MDL_SK_fnc_serverVariables = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_serverVariables.sqf";
MDL_SK_fnc_spawner_add_vehicles = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_spawner_add_vehicles.sqf";
MDL_SK_fnc_spawner_reload = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_spawner_reload.sqf";
//MDL_SK_fnc_template = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_template.sqf";
MDL_SK_fnc_terro_ammo = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_terro_ammo.sqf";
MDL_SK_fnc_terro_check = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_terro_check.sqf";
MDL_SK_fnc_terro_special_ammo_crates = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_terro_special_ammo_crates.sqf";
MDL_SK_fnc_terro_special_ammo = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_terro_special_ammo.sqf";
MDL_SK_fnc_terro_special_ammo_marker = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_terro_special_ammo_marker.sqf";
MDL_SK_fnc_terro_teleport_client = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_terro_teleport_client.sqf";
MDL_SK_fnc_terro_teleport_position_marker = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_terro_teleport_position_marker.sqf";
MDL_SK_fnc_terro_teleport_position_marker_remove = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_terro_teleport_position_marker_remove.sqf";
MDL_SK_fnc_terro_teleport_server = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_terro_teleport_server.sqf";
MDL_SK_fnc_time_clock = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_time_clock.sqf";
MDL_SK_fnc_vehicle_alarm = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_vehicle_alarm.sqf";
MDL_SK_fnc_vehicle_alarm_msg = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_vehicle_alarm_msg.sqf";
MDL_SK_fnc_vehicle_spawn = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_vehicle_spawn.sqf";
MDL_SK_fnc_vehicle_spawn_init = compile preprocessFileLineNumbers "fnc\MDL_SK_fnc_vehicle_spawn_init.sqf";
// = compile preprocessFileLineNumbers "fnc\.sqf";

/*
// ACTION TO STOP CIVILIAN CAR FROM MOVING
MDL_SK_fnc_police_car_action={
	 (_this select 0) addAction ["Zatrzymaj do kontroli", {
		_player = (_this select 1);
		_car = (_this select 0);
		_nearestunits = nearestObjects [_player,["Man","Car"],20]; 
		_wantedUnits = []; 
		{ 
			if (side _x == civilian) then {
				_wantedUnits pushback _x;
			};
		} foreach _nearestunits; 
//		hint str _wantedUnits;//debug 
		playSound3D ["rds_a2port_civ\scripts\police_siren.ogg", _car, false, getPosASL _car, 15, 1, 0]; 
		{
			_responseTime = (ceil random 7) + 3;
			[_x, _responseTime] spawn {sleep (_this select 1); (_this select 0) disableAI "MOVE";};  
			[_x, _responseTime + 60] spawn {sleep (_this select 1); (_this select 0) enableAI "MOVE";};  
		} foreach _wantedUnits;
	} ,	nil, 1, true, true, "", "true", 3, false, ""];
};
*/