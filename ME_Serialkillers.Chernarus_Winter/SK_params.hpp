
class Daytime
{
	title = "$STR_a3_cfgvehicles_moduledate_f_arguments_hour_0";
	values[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23};
	texts[] = {"00:00","01:00","02:00","03:00","04:00","05:00","06:00","07:00","08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00"};
	default = 12;
	function = "BIS_fnc_paramDaytime";
};

class TimeAcceleration
{
	title = "$STR_usract_time_inc";
	values[] = {1,2,5,10,20};
	texts[] = {
		"x1",
		"x2",
		"x5",
		"x10",
		"x20"
	};
	default = 1;
	function = "BIS_fnc_paramTimeAcceleration";
};

class MaxViewDistance
{
	title = "Max View Distance";
	values[] = {500,1000,1500,2000,2500,3000,5000,7500,10000};
	default = 2000;
};

class Snow
{
	title = "Snow";
	values[] = {0,0.5,1};
	texts[] = {"No","Random","Yes"};
	default = 0;
};