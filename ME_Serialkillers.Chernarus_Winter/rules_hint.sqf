_text = [

"Polecamy zapoznac sie z zasadami.<br />",

 "<t underline='true' color='#ff0000'>Zasady</t><br/>
1. Policja nie korzysta ze skrzynek terrorystów, ale może używać tej broni pozostawionej przez terrorystów (również martwych) np. w pojazdach, którymi się poruszali.<br />
2. Policja nie (zabija/molestuje/okalecza) cywili nie mając 100% pewności, że to może być terrorysta.<br />
3. Policja nie zabija więźniów, jeśli nie są uzbrojeni i nie stanowią zagrożenia.<br />
4. Policja nie powinna nadużywać helikopterów (1 powinien wystarczyć...), a w szczególności nie używać ich jako pocisków kinetycznych.<br />
5. Terroryści nie powinni używać policyjnego arsenału ani spawnowania pojazdów.<br />
6. Terroryści powinni zapewnić na tyle akcji, żeby policja nie mogła się nudzić.<br />
7. Jeżdzenie po normalnych drogach (A NIE PO LASACH) na pewno uprzyjemni wszystkim rozgrywkę.<br />
<br />
Dokladne zasady sa dostepne w notatkach mapy (Mapa -> lewy gorny 'Notes').<br />
W razie problemow pisać."

];

_t = [];
{
	_t = _t + [(parseText _x)];
} foreach _text;
_text = _t;

"Arma 3 SerialKillers" hintC _text;