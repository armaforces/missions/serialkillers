params ["_oldUnit", "_killer", "_respawn", "_respawnDelay"];

private _side = player getVariable "Player_Side"; 
if (_side == WEST) exitwith {
	//Find killer to change points based on who killed unit
	_killer = if (isNull _killer) then {
        _oldUnit getVariable ["ace_medical_lastDamageSource", _killer];
   } else { _killer };
   //Send message to server that policeman was killed and who killed him
	[player, _killer] remoteExec ["MDL_SK_fnc_cop_killed", 2];
};

if (_side == EAST) exitWith {
	//Send message to server that terrorist was killed
	[_oldUnit] remoteExec ["MDL_SK_fnc_terro_killed",2];
};