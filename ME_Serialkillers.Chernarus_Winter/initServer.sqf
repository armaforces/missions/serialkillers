sleep 1;
[WEST, 0] call BIS_fnc_respawnTickets;
[EAST, 0] call BIS_fnc_respawnTickets;
//execVM "server\spawn_city.sqf";
[] remoteExec ["MDL_SK_fnc_civs",0];

_start_time = daytime;
/*
onPlayerConnected {
	isJip = _jip;
	if (isJip) then {
		_owner publicVariableClient "SK_Cops";
		_owner publicVariableClient "SK_BLUFOR_Score";
		_owner publicVariableClient "SK_OPFOR_Score";
		_owner publicVariableClient "SK_Civis";
	};
};
*/

sleep 5;
[] spawn MDL_SK_fnc_arsenal;
[] spawn MDL_SK_fnc_terro_teleport_server;
[] spawn MDL_SK_fnc_vehicle_spawn_init;
[] spawn MDL_SK_fnc_terro_ammo;
[] spawn {
	waitUntil {!isNil("SK_serverInitialized")};
	waitUntil {SK_serverInitialized};
	waitUntil {
		private _crates = +SK_Terro_ammoCrates;
		sleep 1;
		if (_crates isEqualTo SK_Terro_ammoCrates) then {
		true;
		} else {
		false;
		};
	};
	[] spawn MDL_SK_fnc_terro_special_ammo_crates;
};

execVM "end.sqf";

[_start_time] spawn {
	waitUntil {daytime >= ((_this select 0) + (1/6))};
	[] spawn MDL_SK_fnc_score_watch;
};

[] spawn MDL_SK_fnc_radio;
[] spawn MDL_SK_fnc_music;

_snow = "Snow" call BIS_fnc_getParamValue;
if (_snow == 1) then {
	SK_snow = true;
	maxviewdistance = 200;
};

sleep 20;
SK_serverInitialized = true;
publicVariable "SK_serverInitialized";